<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('news')->group(function(){
    Route::get('/recent','API\ArticleController@recent');
    Route::get('/categories','API\CategoryController@index');
    Route::get('/','API\ArticleController@recent');
    Route::get('/post/{id}','API\ArticleController@getArticle');
    Route::get('/article','API\ArticleController@posts');
});

Route::get('/fb/webhook',"FacebookWebhookController@page");
Route::post('/fb/webhook',"FacebookWebhookController@page");


Route::prefix('video')->group(function(){
    Route::patch('/update',"API\VideoController@update");
});