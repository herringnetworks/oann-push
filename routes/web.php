<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\DynamicImageController;
use App\Http\Controllers\ElectionMapController;
use App\Http\Controllers\StateController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/push/create','PushArticleController@create')->name('push.create');
Route::post('/push/save','PushArticleController@save')->name('push.save');

Route::post('/registerAdmin','Auth\RegisterController@registerAdmin')->name('user.register');

Route::get('wordpress/post',function() {
    $post = Wordpress::posts();

    return $post->collection();
});

Route::get('/test','ArticleController@index');

Route::get('/live/fb',"FBLiveController@index")->name('live.fb');
Route::post('/live/fb/update',"FBLiveController@update")->name('live.fb.update');
Route::post('/live/fb/restart',"FBLiveController@restart")->name('live.fb.restart');
Route::get('/live/fb/warn',"FBLiveDownController@mailWarining");
Route::post('/live/fb/timer',"FBLiveDownController@setTimer");

Route::get('/video/create',"VideoController@create")->name('video.create');
Route::post('/video/upload',"VideoController@upload");
Route::get('/video/',"VideoController@index")->name('video.index');
Route::get('/video/all',"VideoController@all")->name('video.all');
Route::get('/video/{id}','VideoController@show');
Route::get('/video/{id}/edit','VideoController@edit');
Route::patch('/video/{id}/update','VideoController@update')->name('video.update');

Route::get('/test/article','HomeController@testArticle');

//Route::get('/fb/webhook',"FacebookWebhookController@page");
//Route::post('/fb/webhook',"FacebookWebhookController@page");

Route::prefix('admin')->group(function(){
    Route::get('users','Admin\UserController@index')->name('admin.user.index');

    Route::get('user/{id}/edit','Admin\UserController@edit')->name('admin.user.edit');
    Route::post('user/{id}/update','Admin\UserController@update')->name('admin.user.update');
    Route::resource('role',Admin\RoleController::class);
    
});

Route::prefix('image')->group(function(){
    Route::get('create','ImageController@index')->name('image.uploader');
    Route::post('upload','ImageController@upload')->name('image.upload');
});


Route::prefix('d')->group(function(){
    Route::get('images/{filename}',[DynamicImageController::class,'show']);
});

Route::prefix('map')->group(function(){
   Route::get('index',[ElectionMapController::class,'index']);
});

Route::prefix('election')->group(function(){
    Route::get('map',[ElectionMapController::class,'index']);
    Route::get('states',[StateController::class,'index']);
});

Route::get('test/video',"VideoPlayerController@index");