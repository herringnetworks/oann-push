let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js').vue({version:2})
   .sass('resources/assets/sass/app.scss', 'public/css').options({
      processCssUrls:false
   })
   .copy('resources/assets/images/oan-center-logo.png','public/images')
   .sass('resources/assets/sass/oann/style.scss','public/css/oann')
   .scripts([
      'resources/assets/js/FBLive.js'
   ],'public/js/FBStream.js')
   .js('resources/assets/js/push.js','public/js')
   .styles(['resources/assets/css/emoji.css',
      
      'node_modules/dropzone/dist/dropzone.css'
],'public/css/plugin.css')
   .scripts([
      
      'node_modules/clipboard/dist/clipboard.js',
      'node_modules/feather-icons/dist/feather.js',
      'node_modules/bootstrap/dist/js/bootstrap.bundle.js',
     
   ],'public/js/plugin.js')
   .js('resources/assets/js/oann.js','public/js/oann.js')
   .js('resources/assets/js/article.js','public/js/article.js')
   .js('resources/assets/js/map/main.js','public/js/map/main.js')
   .sass('resources/assets/sass/map/app.scss','public/css/map/app.css')
   .css('resources/assets/css/map/style.css','public/css/map/style.css');