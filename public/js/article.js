/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/assets/js/videoModifier.js":
/*!**********************************************!*\
  !*** ./resources/assets/js/videoModifier.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "addClassToVideos": () => (/* binding */ addClassToVideos),
/* harmony export */   "initVideoModification": () => (/* binding */ initVideoModification),
/* harmony export */   "removeAutoPlayFromVideos": () => (/* binding */ removeAutoPlayFromVideos)
/* harmony export */ });
function addClassToVideos(selector, className) {
  var videos = document.querySelectorAll(selector);
  videos.forEach(function (element) {
    element.classList.add(className);
  });
}
function removeAutoPlayFromVideos(selector) {
  var videos = document.querySelectorAll(selector);
  videos.forEach(function (video) {
    if (video.hasAttribute('autoplay')) {
      video.removeAttribute('autoplay');
    }
  });
}
function initVideoModification() {
  addClassToVideos('video', 'video-js');
  addClassToVideos('video', 'vjs-big-play-centered');
  removeAutoPlayFromVideos('video');
}

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!****************************************!*\
  !*** ./resources/assets/js/article.js ***!
  \****************************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _videoModifier_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./videoModifier.js */ "./resources/assets/js/videoModifier.js");

document.addEventListener('DOMContentLoaded', function () {
  (0,_videoModifier_js__WEBPACK_IMPORTED_MODULE_0__.initVideoModification)();
});
})();

/******/ })()
;