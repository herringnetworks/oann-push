



function getTimeRemaining(endtime){
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor( (t/1000) % 60 );
    var minutes = Math.floor( (t/1000/60) % 60 );
    var hours = Math.floor( (t/(1000*60*60)) % 24 );
    var days = Math.floor( t/(1000*60*60*24) );
    return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}

Date.prototype.addHours = function(h){
    this.setTime(this.getTime() + (h*60*60*1000));
    return this;
};

Date.prototype.addMinutes = function(m){
    this.setTime(this.getTime() + (m * 60 * 1000));
    return this;
};


function initializeClock(id, endtime){
    var clock = document.getElementById(id);

    clock.classList.add('show');

    var daySpan = clock.querySelector('.days');
    var hoursSpan = clock.querySelector(".hours");
    var minutesSpan = clock.querySelector(".minutes");
    var secondsSpan = clock.querySelector(".seconds");

    function updateClock(){
        var t = getTimeRemaining(endtime);
        //clock.innerHTML = t.hours + ':' + ':' + t.minutes + ':' + t.seconds;
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        console.log(t.total);


        if(t.total === 300000){
            clock.classList.remove("green");
            clock.classList.add("red");
            axios.get('/live/fb/warn')
                .then(function(response){
                    console.log(response);
                })
                .catch(function(error){
                    console.log(error);
                });
        }
        //console.log(t.total);
        if(t.total<=0){

            clearInterval(timeinterval);
        }
    }

    updateClock();
    var timeinterval = setInterval(updateClock,1000);
}

function resetTime(){
    var date = new Date();

    //
    // date.addHours(8);
    date.addMinutes(7);
    console.log("ResetTimer");

    axios.post("/live/fb/timer")
        .then(function(response){
            console.log(response);
            initializeClock("clockdiv",date);
        })
        .catch(function(error){
            console.log(error);
        });


}





document.addEventListener('DOMContentLoaded', function(){
    var timerResetButton =  document.getElementById("resetTimer");
    timerResetButton.addEventListener("click",resetTime,false);
}, false);




