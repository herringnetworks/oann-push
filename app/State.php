<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;    

    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['state_name','state_abbreviation','fips_code','electoral_votes'];
}
