<?php 
namespace App\Services;

use Wordpress;
use App\Interfaces\PostInterface;


class WordpressPostAdapter implements PostInterface 
{

  

    public function getPosts($params) 
    {
        return Wordpress::posts($params);
    }

    public function getPost($id,$params)
    {
        return Wordpress::custom('posts/' . $id,$params);
    }
}