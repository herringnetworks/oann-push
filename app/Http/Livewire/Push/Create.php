<?php

namespace App\Http\Livewire\Push;

use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\OanOneSignalSdk\OneSignalSdk;


class Create extends Component
{

    public $title ;
    public $subtitle;
    public $content = "Default Message";
    public $segments;
    public $link;


    protected $rules = [
        'segments' => 'required',
        'title' => 'required|min:5',
        'content' => 'required|min:10',
        'link' => 'url'
    ];

    public function mount()
    {
        // $this->title = "";
        // $this->message = "";
        // $this->subtitle = "";
    }

    public function render()
    {
        return view('livewire.push.create');
    }



    public function submitNotification()
    {
        // $oneSignal->notification()->create([

        // ])

        Log::info($this->getErrorBag());
        $this->validate();
        
        Log::info("Submit has been made");
       
        //$this->resetForm();

        
    }

    public function resetForm()
    {
        $this->title = "";
        $this->content = "";
        $this->subtitle = "";
        $this->segments = "";
    }

    public function resetContent()
    {
        $this->content = "";
    }
}
