<?php

namespace App\Http\Livewire\Video;

use Livewire\Component;

class Show extends Component
{

    public $video;
    public function render()
    {
        return view('livewire.video.show');
    }
}
