<?php

namespace App\Http\Livewire\Video;

use Livewire\Component;

class Update extends Component
{
    public $video;
    public $title;
    public $description;

    protected $rules = [
        'title' => 'required',
        'description' => 'required'
    ];

    public function render()
    {
        return view('livewire.video.update');
    }


    public function updateVideo()
    {
        $validated = $this->validate();
        var_dump($validated);
        $this->video->title = $validated['title'];
        $this->video->description = $validated['description'];
        $this->video->save();
    }

    public function mounted($video)
    {
        //$this->title = $video->title;
        //$this->description = $video->description;

        $this->video = $video;
    }
}
