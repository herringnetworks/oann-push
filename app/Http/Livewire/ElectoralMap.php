<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\State;

class ElectoralMap extends Component
{

    public $states;

    public function mount()
    {
        $this->states = State::all();
    }

    public function render()
    {
        return view('livewire.electoral-map');
    }
}
