<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {

        $users = User::all();
        $roles = Role::all();
        return view('admin.user.index',compact('users','roles'));
    }

    public function edit(Request $request,$id){
        $user = User::findOrFail($id);
       
        return view('admin.user.edit',compact('user'));
    }

    public function update(Request $request,$id)
    {
        
    }
}
