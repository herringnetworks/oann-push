<?php

namespace App\Http\Controllers;

use App\Client\WMSPanel\NimbleStreamer\Api\Control\LiveStreams;
use App\Client\WMSPanel\NimbleStreamer\Api\Control\NimbleMpegts;
use App\Client\WMSPanel\NimbleStreamer\Api\NimbleRoutes;
use App\Oan\OanArticleStorage;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',[
            'except' => [
                'testArticle'
            ]
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function testArticle()
    {
        $nimbleRoute = new NimbleMpegts();
        $nimbleResponse = $nimbleRoute->getOutGoingStreams(env('FB_STREAM_SERVER_ID'));
        $jsonResponse = json_decode($nimbleResponse->getBody(),true);
        return $jsonResponse;
    }



}
