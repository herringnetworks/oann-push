<?php

namespace App\Http\Controllers;

use Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class ImageController extends Controller
{

    const OANN_IMAGE_IMPORTER_URL = "https://www.oann.com/api/image-test.php?";
    const OANN_IMAGE_IMPORTER_DEBUG = "http://wordpress-test.test/api/image-test.php?";

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('image.index');
    }


    public function upload(Request $request)
    {
        $uploadFile = str_replace(" ","-",$request->file->getClientOriginalName());
        $path = Storage::disk('s3:image')->putFileAs('uploads',$request->file,$uploadFile);

        $link = Storage::disk('s3:image')->url($path);
        $response = Http::get(ImageController::OANN_IMAGE_IMPORTER_URL . "url=". urlencode($link));
        var_dump($response);
        return response()->json([
            "message" => "Successful",
            "path" => Storage::disk('s3:image')->url($path),
            "importer_response" => $response
        ]);
    }
}
