<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OanOneSignalSdk\OneSignal;
use Illuminate\Support\Facades\Log;
use App\OanOneSignalSdk\API\Notification;
class PushArticleController extends Controller
{
    //

    public function __construct(){
    	$this->middleware('auth');
    }


    public function create()
    {
    	//return view('push.create');
		return view('push.new');
    }

    public function save(Request $request)
    {
    	$request->validate([
    		'title' => 'required',
    		'content' => 'required'
    	]);

	    $content = array(
	        "en" => $request->input('content')
	    );

	    $headings = array(
	    	"en" => $request->input('title')
	    );
	   
	    $fields = array(
	  
	        'included_segments' => array(
	            'All'
	        ),
	        'data' => array(
	            "type" => "breaking"
	        ),
	        'contents' => $content,
	        'headings' => $headings,
	     
	    );

	    
	    $onesignal = new OneSignal(env('ONE_SIGNAL_AUTH_KEY',null),env('ONE_SIGNAL_API_KEY',null),env('ONE_SIGNAL_APP_ID',null));
	    $noticiation = new Notification($onesignal,env('ONE_SIGNAL_AUTH_KEY',null),env('ONE_SIGNAL_API_KEY',null),env('ONE_SIGNAL_APP_ID',null));

	    $response = json_decode($noticiation->createMobile($fields));
		Log::info(json_encode($response));
	    $noticiation->createWeb($fields);
	   if($response->status_code != '200')
	    {
	    	$status = 'error';
	    	$message = $response->response;
	    }else {
	    	$status = "success";
	    	$message= "Your Breaking News Alert has been Sent!";
	    }
     	return redirect('/push/create')->with($status,$message);
    }

}

