<?php

namespace App\Http\Controllers;


use App\Events\FBLiveVideoStatus;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class FacebookWebhookController extends Controller
{

    public function page(Request $request)
    {


        //dd($request);
        Log::info($request->all());
        if($request->query('hub_verify_token') == "ender123")
        {

            if(isset($request->entry)){
                $changes = $request->entry[0]['changes'][0];
                Log::info($changes);
                if($changes['field'] == "live_videos")
                {
                    Log::info($changes);
                    event(new FBLiveVideoStatus($changes['values']['status']));
                }
            }

            return response($request->query('hub_challenge'),200);
        }

        return response("Error",403);
    }
}
