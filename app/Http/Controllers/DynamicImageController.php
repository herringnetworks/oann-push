<?php

namespace App\Http\Controllers;

use App\Oan\ImageConverter;
use Illuminate\Http\Request;
use App\Oan\ImageLinkGenerator;
use App\Oan\Inferfaces\LinkGenerator;

class DynamicImageController extends Controller
{

    private $imageConverter;
    public function __construct(ImageConverter $imageConverter)
    {
        $this->imageConverter = $imageConverter;
    }
    public function show(Request $request,string $filename)
    {
        $path = $request->path();

        $pathPart = pathinfo($filename);

        $decodedFilename = base64_decode($pathPart['filename']);

        $jsonFileData = json_decode($decodedFilename,true);
       
        //var_dump($jsonFileData);

       
        $imageBinary = $this->imageConverter->webpToPng($jsonFileData['url'],public_path('images/oan/articles'). "/" . date('Y-m-d') . "/" . $pathPart['filename']. ImageConverter::EXTENSION_PNG);

        return $imageBinary;

    }
}
