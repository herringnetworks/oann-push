<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client\WMSPanel\NimbleStreamer\Api\Control\RTMPRepublish;

class FBLiveController extends Controller
{
    protected $rtmpRepublish;


    public function __construct()
    {
        $this->middleware('auth');
        $this->rtmpRepublish = new RTMPRepublish();
    }
    public function index()
    {
        $rtmpRepublish = new RTMPRepublish();
        $ruleResponse = $rtmpRepublish->getRule(env('FB_STREAM_SERVER_ID'),env('FB_STREAM_RULE_ID'));
        $ruleResponseArray = json_decode($ruleResponse->getBody(),true);
        return view("livefeed.fblive")->with('republish',$ruleResponseArray['rule']);
    }

    public function update(Request $request)
    {
        $request->validate([
            'key' => 'required'
        ]);
        $rtmpRepublish = new RTMPRepublish();
        $streamKey = $request->input('key');
        $keyAndParams = explode("?",$streamKey);
        $ruleResponse = $rtmpRepublish->updateRule(env('FB_STREAM_SERVER_ID'),env('FB_STREAM_RULE_ID'),array(
            'dest_strm' => $keyAndParams[0],
            'dest_strm_params' => $keyAndParams[1]
        ));

       return redirect('/live/fb')->with("success",true);

    }

    public function restart(Request $request)
    {
        $rtmpRepublish = new RTMPRepublish();

        $ruleResponse = $rtmpRepublish->restartRule(env('FB_STREAM_SERVER_ID'),env('FB_STREAM_RULE_ID'));

        return redirect('/live/fb')->with("success",true);
    }
}
