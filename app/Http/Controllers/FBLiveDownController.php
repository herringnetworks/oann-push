<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\LiveFeedEnding;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;

class FBLiveDownController extends Controller
{

    public function mailWarining(Request $request)
    {
        Mail::to("ruben.rangel@klowdtv.com")->later(5,new LiveFeedEnding());
    }

    public function setTimer(Request $request){
        $timer  = Carbon::now();
        $timer->addHours(8);

        Redis::set("fbLive",$timer->toDateTimeString());

        return response()->json([
            'success' => true
        ]);
    }
}
