<?php

namespace App\Http\Controllers;

use Auth;
use Storage;
use App\Video;
use Illuminate\Support\Str;
use Illuminate\Http\Request;


class VideoController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $videos = Auth::user()->videos()->orderBy('created_at','desc')->paginate(10);

        
        return view('video.index')->with(compact('videos'));
    }

    public function create()
    {
        return view('video.create');
    }

    public function upload(Request $request)
    {

        $videoUUID = Str::uuid();
        $path = Storage::disk('s3')->putFileAs("",$request->file('file'),$videoUUID.".". $request->file('file')->getClientOriginalExtension());
        Video::create([
            "title" => $request->file('file')->getClientOriginalName(),
            "status" => "pending",
            "uuid" => $videoUUID,
            "user_id" => Auth::id()

        ]);
      

        return response()->json([
            "message" => "Successful"
        ]);
    }

    public function show(Request $request,$id)
    {
        $video = Video::findOrFail($id);

        return view('video.show')->with(compact('video'));
    }

    public function all()
    {
        $videos = Video::orderBy('created_at','desc')->paginate(10);

        return view('video.index')->with(compact('videos'));
    }

    public function edit(Request $request,$id)
    {
        $video = Video::find($id);
        return view('video.edit',compact('video'));
    }


   
}
