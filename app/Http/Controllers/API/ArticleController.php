<?php

namespace App\Http\Controllers\API;

use App\OanArticle;
use Illuminate\Http\Request;
use App\Http\Resources\ArticleResource;
use App\Http\Controllers\Controller;
use App\Oan\ImageConverter;
use App\Oan\Traits\ImageableTrait;
use App\Services\WordpressPostAdapter;
use App\Wordpress\Content\WPArticle;
use Illuminate\Support\Facades\Log;

class ArticleController extends Controller
{
    use ImageableTrait;


    private $imageConverter;


    public function __construct(ImageConverter $imageConverter)
    {
        $this->imageConverter = $imageConverter;
    }


    public function index(Request $request,WordpressPostAdapter $wordpress)
    {
        $category = $request->categories;
        $embed = $request->embed;
        $page = $request->page;

        $posts = $wordpress->getPosts([
            'categories' =>$request->categories,
            '_embed' => $request->embed,
            'page' => $request->page
        ]);

        //var_dump($posts->collection());

        $articles = $posts->collection()->map(function($item,$key){
           //var_dump($item);
            $imageUrl = isset($item['_embedded']['wp:featuredmedia'][0]['source_url'])?$item['_embedded']['wp:featuredmedia'][0]['source_url']:"";
          
            $article = new WPArticle($item['id'],$item['title']['rendered'],$item['excerpt']['rendered'],$item['content']['rendered'],$item['_embedded']['wp:term'][0][0]['name'],$imageUrl);
            return $article->toArray();
        });

        //var_dump($articles);
        
        return $articles->toArray();
    }

    public function posts(Request $request,WordpressPostAdapter $wordpress)
    {
        $category = $request->categories;
        $embed = $request->embed;
        $page = $request->page;

       /*  $posts = OanArticle::where('category_id',$category)
            ->orderBy('created_at','desc')
            ->paginate(10);


        $articles = $posts->map(function($item,$key){
            $imageUrl = isset($item['image_url'])?$item['image_url']:"";
            $excerpt = isset($item['excerpt'])?$item['excerpt']:"";
            $article = new WPArticle($item['wordpress_id'],$item['title'],$excerpt,$item['content'],"",$imageUrl);

            return [$article->toArray();]
        });
 */
        $params = [
            'category' => $category,
            'embed' => $embed,
            'page' => $page
        ];

        $articles = $this->getPosts($params,$wordpress);
        return $articles;    

    }


    

    public function show(Request $request,WordpressPostAdapter $wordpress,$id)
    {
       
        $embed = $request->embed;

        $postResponse = $wordpress->getPost($id,[
        
            '_embed' => $embed
        ]);

        $post = $postResponse->collection()->toArray();

        $imageUrl = isset($post['_embedded']['wp:featuredmedia'][0]['source_url'])?$post['_embedded']['wp:featuredmedia'][0]['source_url']:"";
        $content = $this->usPrivacyFilter($post['content']['rendered']);
        $article = new WPArticle($post['id'],$post['title']['rendered'],$post['excerpt']['rendered'],$content,$post['_embedded']['wp:term'][0][0]['name'],$imageUrl);


        return $article->toArray();
        
    }

    public function getArticle($id)
    {
        return $this->getPost($id);
    }


    private function getPost($id)
    {

        $post = OanArticle::where('wordpress_id', $id)->first();
        $imageUrl = isset($post['image_url']) ? $post['image_url'] : "";
        $excerpt = isset($post['excerpt']) ? $post['excerpt'] : "";
        $content = $this->usPrivacyFilter($post['content']);
        $article = new WPArticle($post['wordpress_id'], $post['title'], $excerpt, $content, "", $imageUrl);
        $article->link = urlencode($post['link']) ;
        return response()->json($article->toArray());
    }


    private function getPosts($params,WordpressPostAdapter $wordpress)
    {
        if(!is_array($params))
        {
            return [];
        }

        $category = $params['category'];
        $embed = $params['embed'];
        $page = $params['page'];

        $posts = OanArticle::where('category_id',$category)
            ->orderBy('created_at','desc')
            ->paginate(10);


        if($posts->isEmpty()){
            $posts = $wordpress->getPosts([
                'categories' =>$category,
                '_embed' => $embed,
                'page' => $page
            ]);
            Log::info("Empty");
            $articles = $posts->collection()->map(function($item,$key){
                $imageUrl = isset($item['_embedded']['wp:featuredmedia'][0]['source_url'])?$item['_embedded']['wp:featuredmedia'][0]['source_url']:"";
                $article = new WPArticle($item['id'],$item['title']['rendered'],$item['excerpt']['rendered'],$item['content']['rendered'],$item['_embedded']['wp:term'][0][0]['name'],$imageUrl);
                return $article->toArray();
            }); 

            return $articles->toArray();
        }else {

            //Log::info("Full");
            $articles = $posts->map(function($item,$key){
                $imageUrl = isset($item['image_url'])?$item['image_url']:"";
            
                /* if(isset($imageUrl) && $this->urlImageType($imageUrl) === IMAGETYPE_WEBP)
                {
                    $imageUrl = $this->imageConverter->webpToPng($imageUrl); 
                    //var_dump($imageUrl);
                    
                }  */
                $excerpt = isset($item['excerpt'])?$item['excerpt']:"";
                $content = $this->usPrivacyFilter($item['content']);
                $link = urlencode($item['link']);
                $article = new WPArticle($item['wordpress_id'],$item['title'],$excerpt,$content,"",$imageUrl);
                $article->link =$link;
                $article->date = $item['created_at'];
                return $article->toArray();
             });
        }

        return $articles->toArray();

    }

    public function usPrivacyFilter($content)
    {
        $search = "let usprivacy = ad_data.us_privacy();";
        $replace = " let usprivacy = \"1---\"";
        
        return str_replace($search,$replace,$content);
    }


    public function recent(Request $request,WordpressPostAdapter $wordpress){
          $category = $request->categories;
        $embed = ($request->embed !== null) ? $request->embed:1;
        $page = ($request->page !== null)? $request->page:1;

       

        if($category === 74197 || $category == "74197" || $category === null){
            $category = 3;
        }

       /*  $posts = OanArticle::where('category_id',$category)
            ->orderBy('created_at','desc')
            ->paginate(10);


        $articles = $posts->map(function($item,$key){
            $imageUrl = isset($item['image_url'])?$item['image_url']:"";
            $excerpt = isset($item['excerpt'])?$item['excerpt']:"";
            $article = new WPArticle($item['wordpress_id'],$item['title'],$excerpt,$item['content'],"",$imageUrl);

            return $article->toArray();
        });
 */
        $params = [
            'category' => $category,
            'embed' => $embed,
            'page' => $page
        ];

        $articles = $this->getPosts($params,$wordpress);
        return $articles;    
    }



}
