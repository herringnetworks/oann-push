<?php

namespace App\Http\Controllers\API;

use App\Video;
use App\Notifications\VideoError;
use App\Notifications\VideoUpdated;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth:api')->except('update');
    }

    public function update(Request $request)
    {
       $video =  Video::where('uuid',$request->uuid)->first();
       $status = strtolower($request->status);
       if($status == 'complete')
       {
            $video->hls = $request->hls;
            $video->url = $request->mp4;
            $video->thumbnail = $request->thumbnail;
       }

       
       $video->status = $status;

       $video->message = $request->message;

       $video->save();
       if($status == 'complete')
       {
            $video->user->notify(new VideoUpdated($video));
       }else 
       {
           $video->user->notify(new VideoError($video));
       }
       

       return response()->json(['message' => 'success']);
    }

    public function edit(Request $request,$id)
    {
        $video = Video::find($id);

        return view('video.edit',compact('video'));
    }
}
