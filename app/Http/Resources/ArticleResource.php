<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ArticleResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($article)
    {
        return [
            'title'=> $article->title,
            'content' => $article->content,
            'feature_image_url' => $article->featureMedia,
            'category' => $article->category,
            'mobile_content' => $article->mobile_content,
            'posted_at' => $article->posted_at,
            'created_at' => $artcile->created_at

        ];
    }
}
