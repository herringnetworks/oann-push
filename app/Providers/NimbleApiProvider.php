<?php

namespace App\Providers;

use App\Client\WMSPanel\NimbleStreamer\NimbleClient;
use Illuminate\Support\ServiceProvider;

class NimbleApiProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Client\WMSPanel\NimbleStreamer\NimbleClient',function($app){
            return new NimbleClient(env("NIMBLE_CLIENT_ID"),env("NIMBLE_API_KEY"));
        });
    }
}
