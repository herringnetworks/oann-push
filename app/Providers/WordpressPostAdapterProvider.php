<?php

namespace App\Providers;

use App\Services\WordpressPostAdapter;
use Illuminate\Support\ServiceProvider;

class WordpressPostAdapterProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('WordpressPostAdapter',function($app){
            return new WordpressPostAdapter();
        });
    }
}
