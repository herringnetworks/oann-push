<?php

namespace App\Providers;

use App\Oan\ImageLinkGenerator;
use Illuminate\Support\ServiceProvider;

class OanImageLinkGeneratorProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ImageLinkGenerator::class,function(){
            return new ImageLinkGenerator(config('oan.dynamicImagesUrl'));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
