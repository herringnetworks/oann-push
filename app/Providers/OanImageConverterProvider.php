<?php

namespace App\Providers;

use App\Oan\ImageConverter;
use Illuminate\Support\ServiceProvider;

class OanImageConverterProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ImageConverter::class,function(){
            return new ImageConverter();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
