<?php

namespace App\Providers;

use App\OanOneSignalSdk\OneSignalSdk;
use Illuminate\Support\ServiceProvider;

class OneSignalSdkServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(OneSignal::class,function() {
            return new OneSignalSdk(config('onesignal'));
        });

        $this->app->singleton(OneSignalSdk::class,function($app){
            return new OneSignalSdk(config('onesignal'));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
