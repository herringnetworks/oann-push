<?php

namespace App\Providers;

use App\Interfaces\PostInterface;
use App\Services\WordpressPostAdapter;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Paginator::useBootstrap();
        $this->app->singleton(PostInterface::class,function($app){
            switch ($app->make('config')->get('services.oann-api-type')){
                case 'wordpress':
                    return new WordpressPostAdapter;

                default:
                    throw new \RuntimeException("Unknown API Client");
            }
        });
    }
}
