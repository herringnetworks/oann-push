<?php

namespace App\Providers;

use App\Oan\OanArticleStorage;
use Illuminate\Support\ServiceProvider;

class OanArticleProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('OanArticleStorage',function($app){
            return new OanArticleStorage();
        });
    }
}
