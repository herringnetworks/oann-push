<?php

namespace App\Listeners;

use App\Events\FBLiveVideoStatus;
use App\Mail\LiveFeedEnding;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FBLiveVideoNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FBLiveVideoStatus  $event
     * @return void
     */
    public function handle(FBLiveVideoStatus $event)
    {
        Mail::to('ruben.rangel@klowdtv.com')->send(new LiveFeedEnding($event->status));
    }
}
