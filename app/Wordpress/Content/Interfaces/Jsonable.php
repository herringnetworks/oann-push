<?php 
namespace App\Wordpress\Content\Interfaces;

interface Jsonable 
{
    public function toJson();
}