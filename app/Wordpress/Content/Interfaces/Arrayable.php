<?php 
namespace App\Wordpress\Content\Interfaces;

interface Arrayable 
{
    public function toArray();
}