<?php 
namespace App\Wordpress\Content;

use App\Wordpress\Content\Interfaces\HtmlInject;
use App\Wordpress\Content\Interfaces\Arrayable;


class WPArticle extends WPPost implements HtmlInject, Arrayable
{
    public $mobile_content;


    public function __construct($id,$title,$excerpt,$content,$category,$featureMedia)
    {
        parent::__construct($id,$title,$excerpt,$content,$category,$featureMedia);
        $this->mobile_content = $this->injectHtml()->render();
    }
    



    public function injectHtml($style = "")
    {

        $title = $this->title;
        $content = $this->content;
        $category = $this->category;
        $featureMedia = $this->featureMedia;
        return view('oan.article',array(
            'title' => $this->title,
            'excerpt' => $this->excerpt,
            'content' => $this->content,
            'category' => $this->category,
            'featureMedia' => $this->featureMedia
        ));
    }

    public function toArray()
    {
        return array(
            'id' => $this->id,
            'title' => $this->title,
            'excerpt' => $this->excerpt,
            'link' => $this->link,
            'content' => $this->content,
            'category' => $this->category,
            'featureMedia' => $this->featureMedia,
            'mobile_content' => $this->injectHtml()->render(),
            'date' => $this->date
        );
    }
}