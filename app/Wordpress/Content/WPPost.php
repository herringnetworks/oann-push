<?php 
namespace App\Wordpress\Content;

use JsonSerializable;
use App\Wordpress\Content\Interfaces\Jsonable;

abstract class WPPost implements JsonSerializable, Jsonable
{
    public $id;
    public $title; 
   // protected $author;
    public $link;
    public $content;
    public $featureMedia;
    public $category;
    public $excerpt;
    public $date;

    public function __construct($id,$title,$excerpt,$content,$category,$featureMedia)
    {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->featureMedia = $featureMedia;
        $this->category = $category;
        $this->excerpt = $excerpt;
       // $this->date = $date;

    }

    public function jsonSerialize()
    {
        return json_encode($this);
    }

    public function toJson()
    {
        return $this->jsonSerialize();
    }
}