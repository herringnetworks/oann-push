<?php 
namespace App\Wordpress\Content;

use App\Wordpress\Content\Interfaces\HtmlInject;
use App\Wordpress\Content\Interfaces\Arrayable;

class OanWPArticle extends ArrayPostAdapter implements HtmlInject,Arrayable 
{
    public function __construct($article)
    {
        parent::__construct($article);
        
    }
}
