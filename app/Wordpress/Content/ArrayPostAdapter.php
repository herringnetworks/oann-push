<?php 
namespace App\Wordpress\Content;

use JsonSerializable;
use App\Wordpress\Content\Interfaces\Jsonable;

abstract class ArrayPostAdapter implements JsonSerializable, Jsonable
{
    public $id;
    public $title; 
   // protected $author;
    public $link;
    public $content;
    public $featureMedia;
    public $category;
    public $excerpt;
    public $created_at;
    public $modified_at;
    public $posted_at;
    //protected $date;

    public function __construct($postArray)
    {
        $this->id = $postArray['wordpress_id'];
        $this->title = $postArray['title'];
        $this->content = $postArray['content'];
        $this->link = $postArray['link'];
        $this->featureMedia = $postArray['image_url'];
        $this->category = $postArray['category'];
        $this->excerpt = $postArray['excerpt'];
        $this->created_at = $postArray['created_at'];
        $this->modified_at = $postArray['modified_at'];
        $this->posted_at = $postArray['posted_at'];
       // $this->date = $date;

    }

    public function jsonSerialize()
    {
        return json_encode($this);
    }

    public function toJson()
    {
        return $this->jsonSerialize();
    }
}