<?php 
namespace App\Oan;

use App\Oan\Traits\ImageableTrait;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Oan\Exceptions\OanFileException;
use App\Oan\Traits\CheckUrlExistanceTrait;
use App\Oan\Interfaces\ImageProcessorInterface;


class ImageConverter implements ImageProcessorInterface{

    const PNG_EXTENSION = 'png';
    const JPG_EXTENSION = 'jpg';
    const WEBP_EXTENSION = 'webp';
    const JPEG_EXTENSION = 'jpeg';
    const GIF_EXTENSION = 'gif';

    const FILESYSTEM_TYPE_S3 = 's3';
    const FILESYSTEM_TYPE_LOCAL = 'local';

    private $config;

    
    use CheckUrlExistanceTrait;
    use ImageableTrait;

    public function __construct($config = null)
    {
        if(isset($config))
        {
            $this->config = $config;
        }else{
            $this->config = [
                'filesystem' => config('oan.filesystem')
            ];
        }
    }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
    public function webpToPng($filepath,$saveTo = null)
    {

        $img = $this->make($filepath,array(
            'extension' => 'webp'
        ));
            
        //$img = imagecreatefromwebp($filepath);

        $whereToSave = (isset($saveTo)) ? dirname($saveTo) : dirname($filepath);

        if(isset($saveTo) && !$this->isUrl($filepath)){
            $whereToSave = dirname($saveTo);
        }else if(!isset($saveTo) && $this->isUrl($filepath))
        {
            $whereToSave = dirname(parse_url($filepath,PHP_URL_PATH));
    
        }else if(isset($saveTo) && $this->isUrl($filepath)){
            $whereToSave = dirname($saveTo);
        }else {
            $whereToSave = dirname($filepath);
        }

        //$saveToFilename = (is_dir($whereToSave))?basename($filepath,$this->getExtension($filepath)):basename($whereToSave,self::PNG_EXTENSION);
        
        if(!isset($saveTo) && (is_dir($whereToSave) || $this->isUrl($filepath)))
        {
           $saveToFilename = basename($filepath,$this->getExtension($filepath));

        }else {
            
           $saveToFilename = basename($saveTo,self::PNG_EXTENSION);
          
        }

        return $this->save($img,$whereToSave . "/" . $saveToFilename . self::PNG_EXTENSION,IMAGETYPE_PNG);
        
    }

    public function make($path,$config = null)
    {
        if ($this->isUrl($path))
            if (!$this->urlExists($path) || !$this->existsInWordpress($path))
                throw new OanFileException("File Url is not found");

        if (!$this->isUrl($path) && !file_exists($path))
            throw new OanFileException("File does not exists in path " . $path);

        $extension = (isset($config) && isset($config['extension'])) ? $config['extension']:$this->getExtension($path);

        $img = null;
        switch ($extension) {
            case self::PNG_EXTENSION:
                $img = imagecreatefrompng($path);
            break;

            case self::WEBP_EXTENSION:
                $img = imagecreatefromwebp($path);

            break;
            default:
                $img = imagecreatefromjpeg($path);

        }

        return $img;

    }

    public function save($resource,$to,$type)
    {

        $imageResult = false;
        $selectedFileSystem = $this->config['filesystem'];

        $whereToSave =  dirname($to);
        
        if(self::FILESYSTEM_TYPE_LOCAL === $selectedFileSystem)
        {

            if (!is_dir($whereToSave) && !file_exists($whereToSave)) {
                mkdir($whereToSave, 0770, true);
            }
            switch ($type) {
                case IMAGETYPE_PNG:
                    $imageResult = imagepng($resource, $to);
                    break;
                case IMAGETYPE_WEBP:
                    $imageResult = imagewebp($resource, $to);
                    break;
            }

            if ($imageResult) {
                return $to;
            }

        }else if(self::FILESYSTEM_TYPE_S3 === $selectedFileSystem)
        {


            if(Storage::disk('s3:image')->exists(ltrim($to,"/"))){
                
                return Storage::disk('s3:image')->url(ltrim($to,"/"));
            }
            ob_start();
            switch($type){
                case IMAGETYPE_PNG:
                    imagepng($resource);
                break;
            }
            $imageRaw = ob_get_contents();

            ob_end_clean();
           
            Storage::disk('s3:image')->put($to,$imageRaw);
            return Storage::disk('s3:image')->url(ltrim($to,"/"));
        }
    

        return false;
    }

    
}