<?php 
namespace App\Oan;

use App\Oan\Interfaces\Fetchable;
use Illuminate\Support\Facades\Http;

class OnDemandService implements Fetchable {

    
    const ONDEMAND_URL = "https://cdn.herringnetwork.com/80A4DFF/OANVODAPI/api/RO/";

    private $onDemandUrl;

    public function __construct()
    {
        
    }

    public function fetch(string $verb,array $parameters = array('api-version' => "1.0"))
    {
        $response = Http::get(self::ONDEMAND_URL . $verb,$parameters);

        return $response;

    }

    public function fetchShows()
    {
        $response = $this->fetch('shows');
        

        return $response;
    }

    /**
     * Fetch the show
     */
    public function fetchShowVideos($id,$parameters = array(['platform' => 'VOD','sortBy' => 'episode','api-version' => "1.0"]))
    {
        $response = $this->fetch('show' ."/" . $id,$parameters);
        return $respond;
    }
}