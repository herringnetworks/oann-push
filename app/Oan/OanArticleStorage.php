<?php


namespace App\Oan;

use App\OanArticle;
use App\Oan\ImageConverter;
use Illuminate\Support\Facades\Log;
use App\Services\WordpressPostAdapter;


class OanArticleStorage
{
    protected $wordpressPostAdapter;
    protected $imageConverter;

    public function __construct(WordpressPostAdapter $wordpressAdapter,ImageConverter $imageConverter)
    {
        $this->wordpressPostAdapter = $wordpressAdapter;
        $this->imageConverter = $imageConverter;
    }

    public function storeArticles()
    {
        $articles = $this->wordpressPostAdapter->getPosts([
            'categories' => 3,
            '_embed' => 1 ,
            'page' => 1
        ])->collection();



        $articles->each(function ($item,$key){

            //dd($item);
            $originalImageUrl = isset($item['_embedded']['wp:featuredmedia'][0]['source_url'])?$item['_embedded']['wp:featuredmedia'][0]['source_url']:"";
            $convertedImageUrl = $originalImageUrl;
            if($this->imageConverter->urlImageType($convertedImageUrl) === IMAGETYPE_WEBP)
            {
                $convertedImageUrl = $this->imageConverter->webpToPng($convertedImageUrl);
            }
            $article = [
                'wordpress_id' => $item['id'],
                'title' => $item['title']['rendered'],
                'excerpt' => $item['excerpt']['rendered'],
                'content' => $item['content']['rendered'],
                'category_id' => (isset($item['categories'][0])?$item['categories'][0]:""),
                'link' => $item['link'],
                'original_feature_image_path' => $originalImageUrl,
                'image_url' => $convertedImageUrl,
                'posted_at' => $item['date'],
                'modified' => $item['modified']
            ];

            $this->saveArticle($article);


        });

        return $articles;
    }

    public function saveCategoryArticles($categories)
    {

        if(!is_array($categories)) 
            return ;

        foreach ($categories as $category)
        {
            $articles = $this->wordpressPostAdapter->getPosts([
                'categories' => $category,
                '_embed' => 1 ,
                'page' => 1
            ])->collection();

            Log::info("Saving Cat: " . $category);



            $articles->each(function ($item,$key) use($category){

                //dd($item);
                
                $originalImageUrl = isset($item['_embedded']['wp:featuredmedia'][0]['source_url']) ? $item['_embedded']['wp:featuredmedia'][0]['source_url'] : "";
                $convertedImageUrl = $originalImageUrl;
                if ($this->imageConverter->urlImageType($convertedImageUrl) === IMAGETYPE_WEBP) {
                    $convertedImageUrl = $this->imageConverter->webpToPng($convertedImageUrl);
                }    
                $article = [
                    'wordpress_id' => $item['id'],
                    'title' => $item['title']['rendered'],
                    'excerpt' => $item['excerpt']['rendered'],
                    'content' => $item['content']['rendered'],
                    'category_id' => $category,
                    'categories' => $item['categories'],
                    'link' => $item['link'],
                    'original_feature_image_path' => $originalImageUrl,
                    'image_url' => $convertedImageUrl,
                    'posted_at' => $item['date'],
                    'modified' => $item['modified']
                ];

                Log::info("Looking At article: " . json_encode($article) );

                $this->saveArticle($article);


            });
        }
        

    }

    public function saveArticle($article)
    {


        if(OanArticle::exists($article['wordpress_id'])->first() == null)
        {
            Log::info("Article doesn't exist in DB Should be saving ......");
            return OanArticle::create($article);
        }elseif(OanArticle::exists($article['wordpress_id'])->isModified($article['modified'])->first() == null){
            // todo add condtion to check if the article is modified and change it 

        }

        return null;
    }
}
