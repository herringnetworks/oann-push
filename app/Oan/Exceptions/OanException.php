<?php 
namespace App\Oan\Exceptions;

use Throwable;

class OanException extends \Exception
{
    public function __construct(string $message, int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message,$code,$previous);
    }
}