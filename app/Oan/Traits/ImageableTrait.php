<?php 
namespace App\Oan\Traits;

use App\Oan\Exceptions\OanFileException;

trait ImageableTrait
{
    use CheckUrlExistanceTrait;

    public function getExtension($filename,$withDot = false): string
    {

        //dd($this->urlExists($filename));
        if(!file_exists($filename) && !$this->isUrl($filename))
            throw new OanFileException("File does not exist");

        

        if(filter_var($filename,FILTER_VALIDATE_URL) !== false)
        {
            $type = $this->urlImageType($filename);
        }else {
            $type = exif_imageType($filename);
        }
        

        return image_type_to_extension($type,$withDot);
    }

    public function getMimeType($filename): string 
    {
        if (!file_exists($filename) || !$this->urlExists($filename))
            throw new OanFileException("File does not exist");

        return image_type_to_mime_type(exif_imagetype($filename));
    }

    public function urlImageType($filename)
    {
        if(!$this->urlExists($filename))
            throw new OanFileException("File does not exist");


        $urlParsed = parse_url($filename);
        $extension = pathinfo($urlParsed['path'],PATHINFO_EXTENSION);
        

        switch($extension)
        {
            case "gif":
                return IMAGETYPE_GIF;
            break;
            case "jpeg":
            case "jpg":
                return IMAGETYPE_JPEG;
            break;
            case "png":
                return IMAGETYPE_PNG;
            break;
            case "swf":
                return IMAGETYPE_SWF;
            break;
            case "psd":
                return IMAGETYPE_PSD;
            break;
            case "BMP":
                return IMAGETYPE_BMP;
            break;
            case "ico":
                return IMAGETYPE_ICO;
            break;
            case "webp":
                return IMAGETYPE_WEBP;
            break;
        }

        return false;

        
    }


    
}