<?php
namespace App\Oan\Traits;

trait CheckUrlExistanceTrait
{
    public function urlExists($url)
    {
        
        try{
            //var_dump($url);
            $headers = @get_headers($url);
            return $headers && strpos($headers[0],'200') !== false;
        }catch(\Exception $e)
        {
            //var_dump($e);
            return false;
        }
    }

    public function isUrl($url)
    {
        return filter_var($url,FILTER_VALIDATE_URL);
    }

    public function existsInWordpress($url)
    {
        $ch = curl_init($url);

        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($ch);
      
        if($response === false)
        {
            return false;
        }

        if(preg_match('/Oops! That page can&rsquo;t be found./i',$response))
        {
            return false;
        }

        curl_close($ch);

        return true;

    }
}