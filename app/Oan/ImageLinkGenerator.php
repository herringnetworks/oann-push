<?php 
namespace App\Oan;

use App\Oan\Interfaces\LinkGenerator;
use App\Oan\Traits\CheckUrlExistanceTrait;

class ImageLinkGenerator implements LinkGenerator 
{

    use CheckUrlExistanceTrait;
    private $filteredImages = [
        'webp'
    ];
    const EXTENSION_PNG = '.png';
    private string $baseUrl = 'http://oann-push.test';


    public function __construct($baseUrl = 'http://oann-push.test')
    {
        $this->setBaseUrl($baseUrl);
    }

    public function generate($url,$type = self::EXTENSION_PNG)
    {
        if($this->shouldGenerate($url))
        {
            if($this->isUrl($url))
            {
                $urlParsed = parse_url($url);
                $filename = pathinfo($urlParsed['path'],PATHINFO_FILENAME);
                $extension = pathinfo($urlParsed['path'],PATHINFO_EXTENSION);

                $json = json_encode([
                    'filename' => $filename,
                    'extension' => $extension,
                    'url' => $url,
                ]);

                $buildUrl = $this->getBaseUrl(). base64_encode($json) . $type;

                return $buildUrl;

            }
        }
    }

    public function shouldGenerate($url)
    {
        if($this->isUrl($url)){
            $url = parse_url($url);
            $extension = pathinfo($url['path'],PATHINFO_EXTENSION);
            return stripos(json_encode($this->filteredImages),$extension) !== false;
        }else{
            $extension = pathinfo($url,PATHINFO_EXTENSION);
            return stripos(json_encode($this->filteredImages),$extension) !== false;
        }
    }

    public function setBaseUrl ($url)
    {
        $this->baseUrl = $url;
    }

    public function getBaseUrl()
    {
        return $this->baseUrl;
    }
}