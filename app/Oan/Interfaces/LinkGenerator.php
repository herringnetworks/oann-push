<?php 
namespace App\Oan\Interfaces;

interface LinkGenerator 
{
    public function generate($url);
}