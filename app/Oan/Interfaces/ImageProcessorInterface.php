<?php
namespace App\Oan\Interfaces;

interface ImageProcessorInterface 
{
    public function make($path,$config);
    public function save($resource,$to,$type);
}