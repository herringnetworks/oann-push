<?php


namespace App\Oan\Interfaces;


use Illuminate\Http\Request;

interface VerifyWebhook
{
    public function verify(Request $request);
}
