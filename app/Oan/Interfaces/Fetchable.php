<?php 
namespace App\Oan\Interfaces;

interface Fetchable {

    public function fetch(string $verb,array $parameters);
}