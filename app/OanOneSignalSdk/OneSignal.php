<?php 
namespace App\OanOneSignalSdk;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\ClientException;

class OneSignal 
{
	
	private $apiKey;
	private $appKey;
	private $authKey;
	private $baseURL;
	private $guzzleClient;
	private $responseHeaders;
	public function __construct($authKey,$appKey = null,$apiKey = null)
	{
		$this->baseURL = 'https://onesignal.com/api/v1/';
		$this->appKey = $appKey;
		$this->apiKey = $apiKey;
		$this->authKey = $authKey;

		$this->guzzleClient = new Client(['base_uri'=>$this->baseURL]);
		
	}


	public function getApiKey()
	{
		return $this->apiKey;
	}

	public function getAppKey()
	{
		return $this->appKey;
	}

	public function getGuzzleClient()
	{
		return $this->guzzleClient;
	}

	public function execute($url,$method,array $options = [])
	{
		$defaultOptions = [
			'headers' => array(
				'Authorization' => sprintf('Basic %s',$this->authKey),
				'Content-Type' => 'application/json'
			),
			'debug' => true
		];
		$endpoint = sprintf('%s%s',$this->baseURL,$url);
		$options = array_replace_recursive($defaultOptions, $options);
		$response = [];
		try {

			$response = $this->guzzleClient->request($method,$endpoint,$options);
		}catch (ClientException $e){
			$response = $e->getResponse();
		}


		return $this->getHeadersContent($response);
	}

	public function getHeadersContent(Response $response)
	{
		$status_code = $response->getStatusCode();
		return (object) [ 
			'status_code' => $status_code,
			'response' => json_decode($response->getBody()->getContents())
		];
	}


}