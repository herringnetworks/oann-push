<?php 
namespace App\OanOneSignalSdk\API;

use App\OanOneSignalSdk\OneSignal;

class Notification 
{

	private $oneSignal;

	private $authKey;
	private $apiKey; 
	private $appKey;

	public function __construct(OneSignal $oneSignal,$authKey,$apiKey = null ,$appKey = null)
	{
		$this->authKey = $authKey;
		$this->apiKey = $apiKey;
		$this->appKey = $appKey;

		$this->oneSignal = $oneSignal;
	}


	public function create(array $notification)
	{
		$notification = array_merge($notification,
			[

			'app_id' => $this->appKey
		]);

		return json_encode($this->oneSignal->execute('notifications','POST',[
			'json' => $notification
		]));
	}

	public function createMobile(array $notification)
	{
		$notification = array_merge($notification,[
			'isIos' => true,
	        'isAndroid' => true
		]);

		return $this->create($notification);
	}

	public function createWeb(array $notification)
	{
		$notification = array_merge($notification,[
			'url' => 'http://oann.com',
			'isAnyWeb' => true
		]);

		return $this->create($notification);
	}


}