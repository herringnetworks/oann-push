<?php 
namespace App\OanOneSignalSdk\Service;

use Illuminate\Support\Arr;
use App\OanOneSignalSdk\Exceptions\InvalidConfigurationException;

abstract class Service {

    protected $config;

    public function __construct(array $config = [])
    {
        if(!Arr::has($config,['api_key','auth_key']))
            throw new InvalidConfigurationException('Missing required credentials [api_key and/or auth_key]');

        $this->config = $config;
    }
}