<?php
namespace App\OanOneSignalSdk;

use App\OanOneSignalSdk\Http\HttpClient;
use App\OanOneSignalSdk\Service\Service;
use App\OanOneSignalSdk\Paths\Notification;


class OneSignalSdk extends Service 
{
    public function notification(): Notification 
    {
        return new Notification(new HttpClient,$this->config);
    }
}