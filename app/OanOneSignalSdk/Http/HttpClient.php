<?php 
namespace App\OanOneSignalSdk\Http;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response;
use App\OanOneSignalSdk\Http\ClientInterface;


class HttpClient implements ClientInterface {

    private $authKey;
    private $apiKey;
    private $baseUrl;

    public function getAuthKey(): string 
    {
        return $this->authKey;
    }

    public function setAuthKey($key): ClientInterface 
    {
        $this->authKey = $key;
        return $this;
    }

    public function getApiKey(): string 
    {
        return $this->apiKey;
    }

    public function setApiKey($apiKey): ClientInterface
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    public function setBaseUrl($url): ClientInterface
    {
        $this->baseUrl = $url;
        return $this;
    }

    public function getBaseUrl(): string 
    {
        return $this->baseUrl;
    }

    public function get(string $url,array $params = []): Response 
    {
        Http::baseUrl($this->getBaseUrl());
        return Http::withHeaders($this->headers())->get($url,$params);
    }

    public function post(string $url,array $data, array $params = []): Response 
    {
        if(empty($this->getBaseUrl())){
            $this->setBaseUrl("https://onesignal.com/api/v1/");
        }
        Http::baseUrl($this->getBaseUrl());
        return Http::baseUrl("https://onesignal.com/api/v1/")->withHeaders($this->headers())->post($url,$data,$params);
    }

    private function headers()
    {
        return [
            'Authorization' => 'Base ' . $this->authKey,
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ];
    }

    
}