<?php 
namespace App\OanOneSignalSdk\Http;

interface ClientInterface 
{
    public function getAuthKey(): string;

    public function setAuthKey(string $authKey): ClientInterface;

    public function getApiKey(): String;

    public function setApiKey(string $apiKey): ClientInterface;

    public function setBaseUrl(string $url): ClientInterface;
    
    public function getBaseUrl(): string;

    public function get(string $url,array $params = []): mixed;

    public function post(string $url,array $data, array $params = []): mixed;


}