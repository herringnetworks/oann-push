<?php 
namespace App\OanOneSignalSdk\Paths;

use App\OanOneSignalSdk\Http\ClientInterface;
use App\OanOneSignalSdk\Exceptions\InvalidArgumentException;

abstract class Path 
{
    protected $config;
    protected $client;

    public function __construct(ClientInterface $client,array $config= [])
    {
        $this->client = $client;
        $this->config = array_merge($config,['app_id' => $config['app_id'] ?? null]);
    }

    public function client(): ClientInterface
    {
        return $this->client->setAuthKey($this->config['api_key'])->setBaseUrl('https://onesignal.com/api/v1/');
    }

    protected function getAppId(string $appId = null): string 
    {
        if(!$appId && !$this->config['app_id'])
            throw new InvalidArgumentException('Missing required Parameter [app_id].');

        return trim($appId ?? $this->config['app_id']);
    }


}