<?php
namespace App\OanOneSignalSdk\Paths;

use App\OanOneSignalSdk\Paths\Path;
use Illuminate\Http\Client\Response;

class Notification extends Path 
{
    
    public function create(array $body): Response
    {
        return $this->client()->post('notifications',
        array_merge([
            'app_id' => $this->getAppId() ],
             $body)
        );
    }
}