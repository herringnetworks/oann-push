<?php
namespace App\OanOneSignalSdk\Exceptions;

use Throwable;

class OneSignalException extends \Exception 
{
    public function __construct(string $message,int $code = 0, Throwable $previous =null)
    {
        parent::__construct($message,$code,$previous);
    }
}