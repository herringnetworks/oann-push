<?php 
namespace App\OanOneSignalSdk\Exceptions;

use App\OanOneSignalSdk\Exceptions\OneSignalException;

class InvalidConfigurationException extends OneSignalException {}