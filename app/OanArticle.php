<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OanArticle extends Model
{
    //
    protected $fillable = ['wordpress_id','title', 'excerpt','content','original_feature_image_path','image_url','category_id','link','posted_at'];

    public function scopeExists($query,$articleId)
    {
        return $query->where('wordpress_id',$articleId);
    }

    public function scopeIsModified($query,$date)
    {
        return $query->where('updated_at','=',$date);
    }
}
