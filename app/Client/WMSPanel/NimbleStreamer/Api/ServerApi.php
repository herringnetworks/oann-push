<?php
namespace App\Client\WMSPanel\NimbleStreamer\Api;

use App\Client\WMSPanel\NimbleStreamer\Api\NimbleApi;

class ServerApi extends NimbleApi{




    public function getServerList()
    {
        $response  = $this->client->instance()->request('GET',$this->version.'/server',[
            'query' => [
                'client_id' => $this->client->clientId(),
                'api_key' => $this->client->apiKey()
            ]
        ]);

        return $response;
    }





}
