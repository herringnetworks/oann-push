<?php
namespace App\Client\WMSPanel\NimbleStreamer\Api\Control;

use App\Client\WMSPanel\NimbleStreamer\Api\NimbleApi;

class RTMPRepublish extends NimbleApi {

    public function getRules($serverId)
    {
        $response = $this->client->instance()->request('GET',$this->version.'/server/'.$serverId.'/rtmp/republish',[
            'query' => [
                'client_id' => $this->client->clientId(),
                'api_key' => $this->client->apiKey()
            ]
        ]);
        return $response;
    }

    public function getRule($serverId,$ruleId)
    {
        $response = $this->client->instance()->request('GET',$this->version.'/server/'.$serverId.'/rtmp/republish/'.$ruleId,[
            'query'=> [
                'client_id'=> $this->client->clientId(),
                'api_key' => $this->client->apiKey()
            ]
        ]);

        return $response;
    }

    public function updateRule($serverId,$ruleId,$updateData)
    {
        $response = $this->client->instance()->request('PUT',$this->version.'/server/'.$serverId.'/rtmp/republish/'.$ruleId,[
            'query' =>[
                'client_id' => $this->client->clientId(),
                'api_key' => $this->client->apiKey()
            ],
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'body' => json_encode($updateData)

        ]);

        return $response;
    }

    public function restartRule($serverId,$ruleId)
    {
        $response = $this->client->instance()->request('GET',$this->version.'/server/'.$serverId.'/rtmp/republish/'.$ruleId.'/restart',[
            'query'=> [
                'client_id' => $this->client->clientId(),
                'api_key' => $this->client->apiKey()
            ]
        ]);

        return $response;

    }
}