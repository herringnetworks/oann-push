<?php


namespace App\Client\WMSPanel\NimbleStreamer\Api\Control;


use App\Client\WMSPanel\NimbleStreamer\Api\NimbleApi;

class NimbleMpegts extends NimbleApi
{

    public function getOutGoingStreams($serverId)
    {
        $response = $this->client->instance()->request('GET',$this->version.'/server/'.$serverId.'/mpegts/outgoing',[
            'query' => [
                'client_id' => $this->client->clientId(),
                'api_key' => $this->client->apiKey()
            ]
        ]);

        return $response;
    }
}
