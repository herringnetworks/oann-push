<?php


namespace App\Client\WMSPanel\NimbleStreamer\Api\Control;


use App\Client\WMSPanel\NimbleStreamer\Api\NimbleApi;

class LiveStreams  extends NimbleApi
{

    public function getStreamsList($serverId)
    {
        $response = $this->client->instance()->request("GET",$this->version.'/server/'.$serverId.'/live/streams',[
            'query' => [
                'client_id' => $this->client->clientId(),
                'api_key' => $this->client->apiKey()
            ]
        ]);

        return $response;
    }
}
