<?php
namespace App\Client\WMSPanel\NimbleStreamer\Api;

use App\Client\WMSPanel\NimbleStreamer\NimbleClient;

class NimbleApi {

    var $client;
    protected $version;
    public function __construct()
    {
        // TODO: DI for this
        $this->client= new NimbleClient(env("NIMBLE_CLIENT_ID"),env("NIMBLE_API_KEY"));
        $this->version = 'v1';
    }
}
