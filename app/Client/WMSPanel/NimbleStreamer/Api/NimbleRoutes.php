<?php


namespace App\Client\WMSPanel\NimbleStreamer\Api;


class NimbleRoutes extends NimbleApi
{

    public function getRoutes()
    {
        $response = $this->client->instance()->request('GET',$this->version . '/routes',[
            'query' => [
                'client_id' => $this->client->clientId(),
                'api_key' => $this->client->apiKey()
            ]
        ]);

        return $response;
    }
}
