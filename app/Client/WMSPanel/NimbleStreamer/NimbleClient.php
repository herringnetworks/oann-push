<?php 
namespace App\Client\WMSPanel\NimbleStreamer;

use GuzzleHttp\Client; 


class NimbleClient {

    protected $client;
    protected $clientId;
    protected $apiKey;


    public function __construct($clientId,$apiKey)
    {
        $this->client = new Client([
            'base_uri' => 'https://api.wmspanel.com'
        ]);

        $this->clientId = $clientId;
        $this->apiKey = $apiKey;
        $this->version = 'v1';
    }

    public function instance()
    {
        return $this->client;
    }

    public function clientId()
    {
        return $this->clientId;
    }

    public function apiKey()
    {
        return $this->apiKey;
    }



}