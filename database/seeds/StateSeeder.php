<?php

namespace Database\Seeders;

use App\State;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
                $states = [
            ['state_name' => 'Alabama', 'state_abbreviation' => 'AL', 'fips_code' => '01', 'electoral_votes' => 9, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Alaska', 'state_abbreviation' => 'AK', 'fips_code' => '02', 'electoral_votes' => 3, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Arizona', 'state_abbreviation' => 'AZ', 'fips_code' => '04', 'electoral_votes' => 11, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Arkansas', 'state_abbreviation' => 'AR', 'fips_code' => '05', 'electoral_votes' => 6, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'California', 'state_abbreviation' => 'CA', 'fips_code' => '06', 'electoral_votes' => 55, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Colorado', 'state_abbreviation' => 'CO', 'fips_code' => '08', 'electoral_votes' => 9, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Connecticut', 'state_abbreviation' => 'CT', 'fips_code' => '09', 'electoral_votes' => 7, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Delaware', 'state_abbreviation' => 'DE', 'fips_code' => '10', 'electoral_votes' => 3, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Florida', 'state_abbreviation' => 'FL', 'fips_code' => '12', 'electoral_votes' => 29, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Georgia', 'state_abbreviation' => 'GA', 'fips_code' => '13', 'electoral_votes' => 16, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Hawaii', 'state_abbreviation' => 'HI', 'fips_code' => '15', 'electoral_votes' => 4, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Idaho', 'state_abbreviation' => 'ID', 'fips_code' => '16', 'electoral_votes' => 4, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Illinois', 'state_abbreviation' => 'IL', 'fips_code' => '17', 'electoral_votes' => 20, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Indiana', 'state_abbreviation' => 'IN', 'fips_code' => '18', 'electoral_votes' => 11, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Iowa', 'state_abbreviation' => 'IA', 'fips_code' => '19', 'electoral_votes' => 6, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Kansas', 'state_abbreviation' => 'KS', 'fips_code' => '20', 'electoral_votes' => 6, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Kentucky', 'state_abbreviation' => 'KY', 'fips_code' => '21', 'electoral_votes' => 8, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Louisiana', 'state_abbreviation' => 'LA', 'fips_code' => '22', 'electoral_votes' => 8, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Maine', 'state_abbreviation' => 'ME', 'fips_code' => '23', 'electoral_votes' => 4, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Maryland', 'state_abbreviation' => 'MD', 'fips_code' => '24', 'electoral_votes' => 10, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Massachusetts', 'state_abbreviation' => 'MA', 'fips_code' => '25', 'electoral_votes' => 11, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Michigan', 'state_abbreviation' => 'MI', 'fips_code' => '26', 'electoral_votes' => 16, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Minnesota', 'state_abbreviation' => 'MN', 'fips_code' => '27', 'electoral_votes' => 10, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Mississippi', 'state_abbreviation' => 'MS', 'fips_code' => '28', 'electoral_votes' => 6, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Missouri', 'state_abbreviation' => 'MO', 'fips_code' => '29', 'electoral_votes' => 10, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Montana', 'state_abbreviation' => 'MT', 'fips_code' => '30', 'electoral_votes' => 3, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Nebraska', 'state_abbreviation' => 'NE', 'fips_code' => '31', 'electoral_votes' => 5, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Nevada', 'state_abbreviation' => 'NV', 'fips_code' => '32', 'electoral_votes' => 6, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'New Hampshire', 'state_abbreviation' => 'NH', 'fips_code' => '33', 'electoral_votes' => 4, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'New Jersey', 'state_abbreviation' => 'NJ', 'fips_code' => '34', 'electoral_votes' => 14, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'New Mexico', 'state_abbreviation' => 'NM', 'fips_code' => '35', 'electoral_votes' => 5, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'New York', 'state_abbreviation' => 'NY', 'fips_code' => '36', 'electoral_votes' => 29, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'North Carolina', 'state_abbreviation' => 'NC', 'fips_code' => '37', 'electoral_votes' => 15, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'North Dakota', 'state_abbreviation' => 'ND', 'fips_code' => '38', 'electoral_votes' => 3, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Ohio', 'state_abbreviation' => 'OH', 'fips_code' => '39', 'electoral_votes' => 18, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Oklahoma', 'state_abbreviation' => 'OK', 'fips_code' => '40', 'electoral_votes' => 7, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Oregon', 'state_abbreviation' => 'OR', 'fips_code' => '41', 'electoral_votes' => 7, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Pennsylvania', 'state_abbreviation' => 'PA', 'fips_code' => '42', 'electoral_votes' => 20, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Rhode Island', 'state_abbreviation' => 'RI', 'fips_code' => '44', 'electoral_votes' => 4, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'South Carolina', 'state_abbreviation' => 'SC', 'fips_code' => '45', 'electoral_votes' => 9, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'South Dakota', 'state_abbreviation' => 'SD', 'fips_code' => '46', 'electoral_votes' => 3, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Tennessee', 'state_abbreviation' => 'TN', 'fips_code' => '47', 'electoral_votes' => 11, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Texas', 'state_abbreviation' => 'TX', 'fips_code' => '48', 'electoral_votes' => 38, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Utah', 'state_abbreviation' => 'UT', 'fips_code' => '49', 'electoral_votes' => 6, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Vermont', 'state_abbreviation' => 'VT', 'fips_code' => '50', 'electoral_votes' => 3, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Virginia', 'state_abbreviation' => 'VA', 'fips_code' => '51', 'electoral_votes' => 13, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Washington', 'state_abbreviation' => 'WA', 'fips_code' => '53', 'electoral_votes' => 12, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'West Virginia', 'state_abbreviation' => 'WV', 'fips_code' => '54', 'electoral_votes' => 5, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Wisconsin', 'state_abbreviation' => 'WI', 'fips_code' => '55', 'electoral_votes' => 10, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['state_name' => 'Wyoming', 'state_abbreviation' => 'WY', 'fips_code' => '56', 'electoral_votes' => 3, 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')]
        ];

        //DB::table('states')->insert($states);
        State::insert($states);
    }
}
