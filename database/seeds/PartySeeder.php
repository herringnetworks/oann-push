<?php

namespace Database\Seeders;

use App\Party;
use Illuminate\Database\Seeder;

class PartySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parties = [
            ['party_name' => 'Democrat', 'party_abbreviation' => "DEM" ,'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['party_name' => 'Republican', 'party_abbreviation' => "REP", 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['party_name' => 'Green', 'party_abbreviation' => "GRN", 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')],
            ['party_name' => 'Libertarian', 'party_abbreviation' => 'LIB', 'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')]
        ];

        Party::insert($parties);


    }
}
