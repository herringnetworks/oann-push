<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOanArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oan_articles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("wordpress_id")->unsigned()->index();
            $table->string("title");
            $table->text('excerpt')->nullable();
            $table->longText('content');
            $table->string('image_url')->nullable();
            $table->integer('category_id')->unsigned()->index()->nullable();
            $table->string('link');
            $table->timestampTz("posted_at");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oan_articles');
    }
}
