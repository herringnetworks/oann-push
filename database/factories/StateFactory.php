<?php

namespace Database\Factories;

use App\State;
use Illuminate\Database\Eloquent\Factories\Factory;

class StateFactory extends Factory
{


    protected $model = State::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
              'state_name' => $this->faker->state(),
              'state_abbreviation' => $this->faker->stateAbbr(),
              'electoral_votes' => $this->faker->randomNumber(54,false),
              'election_year' => $this->faker->year(),
              'fips_code' => $this->faker->randomNumber(51,false)
        ];
    }
}
