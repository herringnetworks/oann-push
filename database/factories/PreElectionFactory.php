<?php

namespace Database\Factories;

use App\PreElectionProjection;
use Illuminate\Database\Eloquent\Factories\Factory;

class PreElectionFactory extends Factory
{
    
    protected $model = PreElectionProjection::class;
    
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
