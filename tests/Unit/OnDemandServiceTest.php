<?php

namespace Tests\Unit;

use App\Oan\OnDemandService;
use PHPUnit\Framework\TestCase;
use Illuminate\Support\Facades\Http;

class OnDemandServiceTest extends TestCase
{



    /**
     * Testing fetching shows 
     * @test
     * @return void
     */
    public function can_fetch_ondemand_shows ()
    {

        Http::fake([
            env('OAN_ONDEMAND_API') . '/shows?api-version=1.0' => 
            Http::response(json_decode(file_get_contents("tests/Stubs/shows_200.json"),true),200)
        ]);

        $onDemandService = new OnDemandService();

        $response = $onDemandService->fetchShows();


        $this->assertEquals(200,$response->getStatusCode());
        $this->assertArrayHasKey("payload",$response->json());

    }


    /**
     * @test
     */
    public function can_fetch_ondemand_shows_live() 
    {
        $onDemandService = new OnDemandService();

        $response = $onDemandService->fetchShows();

        $this->assertEquals(200,$response->getStatusCode());
        $this->assertArrayHasKey("payload",$response->json());
    }
    
}
