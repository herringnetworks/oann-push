<?php

namespace Tests\Unit;

use Tests\TestCase;

use App\OanOneSignalSdk\OneSignalSdk;
use App\OanOneSignalSdk\Paths\Notification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\OanOneSignalSdk\Exceptions\OneSignalException;

class OneSignalSdkTest extends TestCase
{


    public $oneSignal;

    public function setUp(): void 
    {
        $config = ['api_key' => '123', 'auth_key' =>'4533'];
        $this->oneSignal = new OneSignalSdk($config);
    }

    /**
     * Checks if notification is created.
     *
     * @test
     * @return void
     */
    public function can_create_notification()
    {
        $expected = $this->oneSignal->notification();

        $this->assertInstanceOf(Notification::class,$expected);
    }


    /**
     *  Checks if exception is thrown for no config.
     *  @test
     */
    public function check_for_no_api_key_exception() {
        $this->expectException(OneSignalException::class);
        new OneSignalSdk();
    }
}
