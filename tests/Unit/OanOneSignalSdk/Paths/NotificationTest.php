<?php

namespace Tests\Unit\OanOneSignalSdk\Paths;

use PHPUnit\Framework\TestCase;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use App\OanOneSignalSdk\Http\HttpClient;
use App\OanOneSignalSdk\Paths\Notification;
use App\OanOneSignalSdk\Http\ClientInterface;

class NotificationTest extends TestCase
{


    public $client;
    public $notification;

    /**
     * Undocumented variable
     *
     * @var array
     */
    public  $config = [
        'app_id' => 'testing-app-id',
        'api_key' => 'testing-api-key',
        'auth_key' => 'testing-auth-key'
    ];

    public function setUp(): void 
    {
        $this->client = \Mockery::mock(ClientInterface::class);
        $this->notification = new Notification($this->client,$this->config);

        //$this->client->expects()->setAuthKey($this->config['api_key'])->once()->andReturn($this->client);
        //$this->client->expects()->setBaseUrl('https://onesignal.com/api/v1/')->once()->andReturn($this->client);
    }

    

    /**
     * Create a notification
     * 
     * @test
     *
     * @return void
     */
    public function can_create_a_notification_mock()
    {

        $data = [
        
            'included_segments' => 'Test Users',
            'contents' => [
                'en' => "this is a notification to all users"
            ],
            'heading' => [
                'en'=> 'This is a test'
            ],
            'url' => "https://oann.com/"
            ,

        ];

        $response = \Mockery::mock(Response::class);

        $this->client->expects()->setAuthKey($this->config['api_key'])->once()->andReturn($this->client);
        $this->client->expects()->setBaseUrl('https://onesignal.com/api/v1/')->once()->andReturn($this->client);


        $this->client->expects()->post('notifications',array_merge([
            'app_id' => $this->config['app_id']
        ],$data))->once()->andReturn($response);

        $typeOf = $this->notification->create($data);

        //dd($typeOf);

        $this->assertTrue(true);
       
       // $this->assertTrue('Illuminate\Http\Client\Response',$typeOf);
    }

    /**
     * @test
     */
    public function can_create_notification_dummy_valid() 
    {
        Http::fake([
            'onesignal.com/api/v1/notifications' => Http::response(
                json_decode(
                    file_get_contents('tests/Stubs/onesignalsdk/notification_create_200.json'),
                    true),
                200)
        ]);

        $http = new HttpClient();

        $notification = new Notification($http,[
            'app_id' => '3dafbfb4-c98b-47f2-b7c2-0bf087623d4e',
            'api_key' => 'OGI0ZThjZDAtZTk3MC00NjlhLWFkODAtN2Q0ZjRiOTZjZGVm'
        ]);

        

        $response = $notification->create( [
            'app_id' => $this->config['app_id'],
            'included_segments' => 'Test Users',
            'contents' => [
                'en' => "this is a notification to all users"
            ],
            'heading' => [
                'en'=> 'This is a test'
            ],
            'url' => "https://oann.com/"
            ,
        ]);
       
        $this->assertEquals(200,$response->getStatusCode());
        $this->assertArrayHasKey('recipients',$response->json());

    }


    /**
     * Test for invalid input
     * @test
     * @return void
     */
    public function cannot_create_notification_dummy_invaid()
    {
        Http::fake([
            'onesignal.com/api/v1/notifications' => Http::response(
                json_decode(
                    file_get_contents('tests/Stubs/onesignalsdk/notification_create_invalid_id_200.json'),
                true),
            200)
        ]);

        $http = new HttpClient();
        $notification = new Notification($http,[
            'app_id' => '3dafbfb4-c98b-47f2-b7c2-0bf087623d4e',
            'api_key' => 'OGI0ZThjZDAtZTk3MC00NjlhLWFkODAtN2Q0ZjRiOTZjZGVm'
        ]);

        $response = $notification->create([
            'app_id' => $this->config['app_id'],
            'app_id' => $this->config['app_id'],
            'included_segments' => 'Test Users',
            'contents' => [
                'en' => "this is a notification to all users"
            ],
            'heading' => [
               
            ],
            'url' => "https://oann.com/",
        ]);

        //dd($response->getStatusCode());
        $this->assertEquals(200,$response->getStatusCode());
        
      // $this->assertArrayHasKey('errors',$response->json());
    }


    public function tearDown(): void 
    {
        \Mockery::close();
        $this->notification = null;
        $this->client = null;
    }
}
