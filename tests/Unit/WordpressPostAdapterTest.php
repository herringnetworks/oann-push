<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Services\WordpressPostAdapter;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WordpressPostAdapterTest extends TestCase
{
    /**
     * A basic test example.
     * @test
     * @return void
     */
    public function can_retetrive_post()
    {
        $this->withoutExceptionHandling();
        $wordpressPost = new WordpressPostAdapter();
        $post = $wordpressPost->getPosts([
            'categories' => 3,
            '_embed' => 1,
            'page' => 1
        ]);
        //dd($post->collection());
        
        $this->assertArrayHasKey('title',$post->collection()->first());
    }
}
