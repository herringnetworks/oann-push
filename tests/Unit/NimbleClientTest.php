<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Client\WMSPanel\NimbleStreamer\NimbleClient;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NimbleClientTest extends TestCase
{
    /**
     * A basic test example.
     * @test
     * @return void
     */
    public function instance_is_of_guzzle()
    {

        $nimbleClient = new NimbleClient(env('NIMBLE_CLIENT_ID',""),env('NIMBLE_API_KEY',""));
        $this->assertInstanceOf(\GuzzleHttp\Client::class,$nimbleClient->instance());
    }
}
