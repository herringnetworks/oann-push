<?php

namespace Tests\Unit\Oan;

use App\Oan\OanArticleStorage;

//use PHPUnit\Framework\TestCase;
use Tests\TestCase;

class OanArticleStorageTest extends TestCase
{


    private $oanArticleStorage;

    public function setUp(): void
    {
        parent::setUp();

        $this->oanArticleStorage = $this->app->make('App\Oan\OanArticleStorage');
    }


    /**
     * @test
     */
    public function can_save_article() 
    {
        $article = [
            'wordpress_id' => '344933988',
            'title' => 'One-on-One with John Zadrozny, America First Legal',
            'excerpt' => null,
            'content' => '<h5>OAN Newsroom<br />
UPDATED 4:10 PM PT –Monday, September 19, 2022</h5>
<p>The Governor of California has waged war against his political opponents by asking the DOJ to investigate the Governors of Florida and Texas to see if they committed a federal crime, like kidnapping and human trafficking by sending migrants to Martha&#8217;s Vineyard and Washington D.C.</p>
<p>What does the law say about this? John Zadrozny with America First Legal joins OAN to discuss whether or not Newsom has a case.</p>

   
    <link rel="stylesheet" href="https://cdn.plyr.io/3.6.4/plyr.css" />
    <script src="https://cdn.plyr.io/3.6.4/plyr.js"></script>
',
    'category_id' => '3',
    'link' => 'https://www.oann.com/cracked-iphone-back-glass/',
    'image_url' => 'https://www.oann.com/wp-content/uploads/2023/03/bidenvisitsCA.webp',
    'posted_at' => '2022-09-19 10:10:55',
    'modified' => '2022-09-19 10:10:55'

];
      $articleSaved = $this->oanArticleStorage->saveArticle($article);  
      //var_dump($articleSaved);

      //$this->assertArrayHasKey('title',$articleSaved);
      $this->assertDatabaseHas('oan_articles',
      [
        'wordpress_id' => '344933988'
      ]);
    }

    /**
     * A basic unit test example.
     * @test 
     * @return void
     */
    public function can_store_articles_from_api()
    {
        $storedArticles = $this->oanArticleStorage->storeArticles();
       
        $this->assertIsArray($storedArticles->toArray());
       
    }
}
