<?php

namespace Tests\Unit\Oan;

use App\Oan\Exceptions\OanFileException;
//use PHPUnit\Framework\TestCase;
use Tests\TestCase;

use App\Oan\ImageConverter;

class ImageConverterTest extends TestCase
{
    /**
     * Test if image does convert
     * 
     * @test
     * @return void
     */
    public function can_convert_from_path_to_png()
    {
        $config = [
            'filesystem' => 'local'
        ];
        $imageConverter = new ImageConverter($config);

        $image = $imageConverter->webpToPng("./tests/Unit/Oan/images/putin-web.webp");

        $this->assertEquals($image, './tests/Unit/Oan/images/putin-web.png');
        $this->assertFileExists('./tests/Unit/Oan/images/putin-web.png');
        //$this->assertTrue($image);

       
    }

    /**
     * @test
     */
    public function can_convert_from_url_to_png() 
    {
        $config = [
            'filesystem' => 'local'
        ];
        $imageConverter = new ImageConverter($config);
        $pathToSave = dirname('./tests/Unit/Oan/images/GettyImages-52120507.webp') ."/". basename('./tests/Unit/Oan/images/GettyImages-52120507.webp','.webp') . '.' .ImageConverter::PNG_EXTENSION;

        $img = $imageConverter->webpTopng('https://www.oann.com/wp-content/uploads/2024/10/GettyImages-1170593.webp',$pathToSave);
        $this->assertFileExists($pathToSave);
        $this->assertEquals($img,$pathToSave);
        //$this->assertTrue($img);
    }


    /**
     * Undocumented function
     * @test
     * @return boolean
     */
    public function can_convert_from_path_to_directory_png()
    {
        $config = [
            'filesystem' => 'local'
        ];
        $imageConverter = new ImageConverter($config);
        $pathToSave = './tests/Unit/Oan/images/' . date('Y') . '/' . date('d') . '/' . 'putin-'.date('Y-d-m').'.png';
        $img = $imageConverter->webpToPng('./tests/Unit/Oan/images/putin-web.webp',$pathToSave);
        $this->assertFileExists($pathToSave);
        $this->assertEquals($img,$pathToSave);
        //$this->assertTrue($img);
    }

    /**
     * @test
     */
    public function thows_exception_from_url_error()
    {
        $config = [
            'filesystem' => 'local'
        ];
        $imageConverter = new ImageConverter($config);
        $pathToSave = dirname('./tests/Unit/Oan/images/GettyImages-52120507.webp') . "/" . basename('./tests/Unit/Oan/images/GettyImages-52120507.webp', '.webp') . '.' . ImageConverter::PNG_EXTENSION;
        $this->expectException(OanFileException::class);
        $img = $imageConverter->webpTopng('https://www.oann.com/wp-content/uploads/2024/10/GettydImages-1170593.webp', $pathToSave);
       
    }

    /**
     * @test
     */
    public function throw_exception_from_path_error() 
    {
        $config = [
            'filesystem' => 'local'
        ];
        $imageConverter = new ImageConverter($config);
       
        $this->expectException(OanFileException::class);
        $img = $imageConverter->webpTopng("./tests/Unit/Oan/images/pfdsutin-web.webp");
    }

    /**
     * @test
     */
    public function can_make_webp_gdimage() 
    {
        $config = [
            'filesystem' => 'local'
        ];
        $imageConverter = new ImageConverter($config);
        $image = $imageConverter->make('./tests/Unit/Oan/images/putin-web.webp');

        $this->assertInstanceOf(\GdImage::class,$image);

    }

    /**
     * @test
     */
    public function can_make_png_resource() 
    {
        $imageConverter = new ImageConverter();
        $image = $imageConverter->make('./tests/Unit/Oan/images/putin-web.png');
        $this->assertInstanceOf(\GdImage::class,$image);
    }
}
