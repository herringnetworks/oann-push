<?php

namespace Tests\Unit\Oan;

use App\Oan\ImageLinkGenerator;
use PHPUnit\Framework\TestCase;

class ImageLinkGeneratorTest extends TestCase
{
    /**
     * @test
     */
    public function check_if_path_should_be_filtered()
    {
        $linkGen = new ImageLinkGenerator(env('OAN_DYNAMIC_IMAGES_URL'));

        $testLink = "wp-content/uploads/2023/02/GettyImages-758944.webp";

        $shouldGen = $linkGen->shouldGenerate($testLink);

        $this->assertTrue($shouldGen);
    }

    /**
     * @test
     */
    public function check_if_url_should_be_filtered() 
    {
        $linkGen = new ImageLinkGenerator(env('OAN_DYNAMIC_IMAGES_URL'));
        $testLink = "https://c3.oann.com/wp-content/uploads/2023/02/GettyImages-758944.webp";

        $shouldGen = $linkGen->shouldGenerate($testLink);

        $this->assertTrue($shouldGen);
    }

    /**
     * @test
     */
    public function can_create_generate_image_link() 
    {
        $linkGen = new ImageLinkGenerator(env('OAN_DYNAMIC_IMAGES_URL'));

        $testLink = "https://c3.oann.com/wp-content/uploads/2023/02/GettyImages-758944.webp";

        $link = $linkGen->generate($testLink);


        $isurl = filter_var($link,FILTER_VALIDATE_URL) !== false;
        $this->assertTrue($isurl);
    }
}
