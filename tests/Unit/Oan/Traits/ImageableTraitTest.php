<?php

namespace Tests\Unit\Oan\Traits;

use App\Oan\Traits\ImageableTrait;
use PHPUnit\Framework\TestCase;

class ImageableTraitTest extends TestCase
{
    /**
     * Tests the get extentions from filename.
     * @test
     * @return void
     */
    public function can_get_png_type_from_path()
    {
        $imagePath = "./tests/Unit/Oan/images/putin-web.webp";
        $dummyclass = new Class {
            use ImageableTrait;
        };

        $extension = $dummyclass->getExtension($imagePath);

        $this->assertEquals('webp',$extension,"actual value is not equals");
    }


    /**
     * @test
     */
    public function can_get_wepb_type_from_url() 
    {
        $imagePath = "https://www.oann.com/wp-content/uploads/2024/10/GettyImages-1170593.webp";
        $dummyClass = new Class {
            use ImageableTrait;
        };

        $extension = $dummyClass->getExtension($imagePath);

        $this->assertEquals('webp',$extension);
    }


   
      
}
