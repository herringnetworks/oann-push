<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Database\Seeders\PartySeeder;
use Tests\TestCase;


class PartyTest extends TestCase
{

    use RefreshDatabase;

    /**
     * Test to see if Seeder is being loaded
     *
     * @return void
     */

    public function test_can_load_party_seeder()
    {
        $this->seed(PartySeeder::class);

        $this->assertDatabaseHas('parties',[
            'party_name' => "Republican",
            'party_abbreviation' => 'REP'
        ]);
    }
}
