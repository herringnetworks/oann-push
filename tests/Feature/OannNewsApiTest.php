<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OannNewsApiTest extends TestCase
{
    /**
     * A basic test example.
     * @test
     * @return void
     */
    public function get_articles_from_category()
    {

        //Get the  

        $response = $this->json('GET','/api/news',[
            'categories' => 3,
            'embed' => 1,
            'page' => 1
        ]);

       //dd($response);

        $response->assertStatus(200)->assertJsonStructure([
            [
                'title',
                'content',
                'category',

            ]
        ]);


       
    }
}
