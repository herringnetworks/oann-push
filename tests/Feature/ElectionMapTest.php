<?php

namespace Tests\Feature;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Database\Seeders\StateSeeder;
use Tests\TestCase;

class ElectionMapTest extends TestCase
{

    use RefreshDatabase;


    /**
     * Tests if seeding is actually seeding states
     * @return void 
     */

    public function test_state_seeding()
    {
        $this->seed(StateSeeder::class);

        $this->assertDatabaseCount('states',50);
        $this->assertDatabaseHas('states',[
            'state_name' => 'Alaska',
            'state_abbreviation' => 'AK',
            'fips_code' => '02',
            'electoral_votes' => 3

        ]);
        
        $this->assertDatabaseHas('states',[
            'state_name' => 'Wyoming',
            'state_abbreviation' => 'WY',
            'fips_code' => '56',
            'electoral_votes' => 3

        ]);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_show_map_with_states()
    {
        $response = $this->get('/election/map');
        
        $response->assertStatus(200);
    }
}
