<?php

namespace Tests\Feature\Api;

use App\Oan\Traits\ImageableTrait;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ArticleControllerTest extends TestCase
{
  use ImageableTrait;  
  /**
   * @test
   */
  public function can_access_recent_ok() 
  {
      $response = $this->get('api/news/recent');
      $response->assertStatus(200);
  }

  /**
   * @test
   */
  public function can_retrieve_recent_articles() 
  {
      $response = $this->get('api/news/recent');

      $response->assertStatus(200);
      $response->assertJsonStructure([
        "*" => [
            "id",
            "title",
            "excerpt",
            "link",
            "content",
            "category",
            "featureMedia",
            "mobile_content",
            "date"
        ]
      ]);
  }

  /**
   * @test
   */
  public function can_change_webp_image_links_to_png() 
  {
      $response = $this->get('api/news/recent');

      $json = $response->json();
      $hasFiltered = false;
      foreach($json as $article)
      {
       
        if(isset($article['featureMedia']) && $article['featureMedia'] !== false){
            var_dump($article['featureMedia']);
            $mediaType = $this->urlImageType($article['featureMedia']);
           
          
            if ($mediaType === "webp") {
                $hasFiltered = true;
                //var_dump("It does have images");
            }
        }
        
      }

      

      $response->assertStatus(200);

      $this->assertFalse($hasFiltered);
      
      
  }
}
