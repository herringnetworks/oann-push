export function addClassToVideos(selector,className){
    const videos = document.querySelectorAll(selector);

    videos.forEach((element) => {
        element.classList.add(className);
    });
}


export function removeAutoPlayFromVideos(selector){
    const videos = document.querySelectorAll(selector);

    videos.forEach( video => {
        if(video.hasAttribute('autoplay')){
            video.removeAttribute('autoplay');
        }
    });
}


export function initVideoModification() {
    addClassToVideos('video', 'video-js');
    addClassToVideos('video','vjs-big-play-centered');
    removeAutoPlayFromVideos('video')
}