const axios = require('axios');

//const CLIENT_ID = "73a5c6ea-478f-498e-abc1-38e947ea6eeb";
//const API_KEY = "2bc980b92d738141282ac8fdc257e7af";


class NimbleClient {
  
    constructor(client_id,api_key){
        this.axiosInstance = axios.create({
            baseUrl:"https://api.wmspanel.com/v1/",

        }) ;

        this.CLIENT_ID = client_id;
        this.API_KEY = api_key;
    }


    servers(){
        this.axiosInstance.get('/server',{
            params: {
                client_id: this.CLIENT_ID,
                api_key:this.API_KEY
            }
        }).then(function(response){
            return response;
        });
    }
}


export default NimbleClient;