<div class="mb-3 position-relative" ">
    <div class="input-group emoji-picker-group" x-data="emojiPicker()" >
        <input type="text" class="form-control input-remove-right-border @error('title') is-invalid @enderror" placeholder="Title" name="title" x-model="title" wire:model="title"  x-ref="emojiInput" x-init="initPicMo($refs.emojiInput)" id="{{$inputId}}" />
        
        <button  type="button" class="btn btn-outline-emoji emoji-picker-btn" ><span class="bi bi-emoji-smile-fill" ></span></button>

        <div class="pickerContainer" x-show="showPicker" ></div>
        
        @error('title')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror

    </div>
</div>


@push('bottom-scripts')

<script src="https://cdn.jsdelivr.net/npm/emoji-mart@latest/dist/browser.js"></script>
<script src="https://unpkg.com/@picmo/popup-picker@latest/dist/umd/index.js"></script>


<script>
    const pickerOptions = { onEmojiSelect: function(emoji,e) {
      console.log({{$inputId}})
      var inputItem = {{ $inputId }};
      console.log(inputItem.value);
      inputItem.value = inputItem.value + emoji.native;
    },
    onClickOutside: function(e){
        //console.log(e);
        console.log(this.element.parentElement.remove());
    },
   
}; 

    const emojiPickerPlace = new EmojiMart.Picker(pickerOptions);
  

    

    function initPicker(options){
        const emojiPicker = new EmojiMart.Picker(options);
        return emojiPicker;
    }
 </script>


@endpush