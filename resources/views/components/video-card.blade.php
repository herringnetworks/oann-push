@props([
    'video-url' =>'',
     'video-format' => ''

])


<div class="card">
      <video 
       
        controls
        preload="auto"
        width="100%"
        height="auto"  >
            <source src="{{ $videoUrl }}" type="video/mp4" />
      </video>

      <div class="card-body">
          {{ $slot }}
      </div>
</div>