@props([
    'header' => ''
])


<div class="card">
    <div class="card-header">
        {{ $header }}
    </div>
    <div class="card-body bg-white">
        {{ $slot }}
    </div>
</div>