@extends("layouts.app")

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <x-card header="Video">

                <div class="row pl-3">
                    <h3>{{ $video->title }}<h3>
                    <span class="ml-3 badge badge-pill  @if($video->status === 'complete') badge-success @elseif($video->status === 'error') badge-danger @else badge-secondary @endif">{{ $video->status }} </span>
                </div>
                
        
                    <div class="video-wrapper">
                        <video 
                        class="video-js"
                        controls
                        preload="auto"
                        width="100%"
                        height="auto" >
                            <source src="{{ $video->url }}" type="video/mp4" />
                        </video>
                    </div>



                <h4>Url</h4>   
                <div class="input-group mb-3  ">
                    
                    <input type="text" id="url" class="form-control" readonly value="{{ $video->url ?? '' }}">
                    <div class="input-group-append">
                        <button class="input-group-text btn-copy" data-clipboard-target="#url">
                            copy
                        </button>
                    </div>
                </div>

                <h4>HLS</h4>
                <div class="input-group mb-3  ">
                    
                    <input type="text" class="form-control" id="hls" readonly value="{{ $video->hls ?? '' }}">
                    <div class="input-group-append">
                        <button class="input-group-text btn-copy " data-clipboard-target="#hls">
                            copy
                        </button>
                    </div>
                </div>


                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="wordpress-tab" data-bs-toggle="tab" data-bs-target="#wordpress" href="#wordpress" role="tab" aria-controls="wordress" aria-selected="true">Wordpress</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="html-tab" data-bs-toggle="tab" data-bs-target="#html" href="#html" role="tab" aria-controls="profile" aria-selected="false">HTML Embed (other sites)</a>
                    </li>
                 
                </ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="wordpress" role="tabpanel" aria-labelledby="home-tab">




                <div class="ps-3 mb-3 mt-3">
                    <h4>Wordpress Script</h4>
                    <button class="btn  btn-copy ml-2 btn-sm btn-outline-primary" data-clipboard-target="#script"><i data-feather="clipboard"></i> Copy Script</button>
                </div>
                
                <div class="wordpress-script">
                    <div class="wordpress-code">

                        <pre id="script" >
<code class="html">
[oann_video src=&quot;{{ $video->url }}&quot; id=&quot;{{ $video->uuid }}&quot;]

</code>

</pre>

</div>
                </div>

  </div>
  <div class="tab-pane fade" id="html" role="tabpanel" aria-labelledby="html-tab">




                <div class="ps-3 mb-3 mt-3">
                    <h4>Html Embed Script</h4>
                    <button class="btn  btn-copy btn-sm btn-outline-primary" data-clipboard-target="#html-script"><i data-feather="clipboard"></i> Copy Script</button>
                </div>
                
                <div class="wordpress-script">
                    <div class="wordpress-code">

                        <pre id="html-script" >
<code class="html">
    
&lt;link href=&quot;https://oann-push.s3-us-west-2.amazonaws.com/skins/treso/videojs.min.css&quot; rel=&quot;stylesheet&quot; &gt 
    
&lt;video id='v-{{ $video->uuid }}'  class=&quot;video-js vjs-theme-city&quot; controlsList=&quot;nodownload&quot style=&quot;width:100%; height:100%;&quot; width=&quot;100%&quot; height=&quot;100%&quot; preload=&quot;auto&quot; controls  data-viblast-key=&quot;N8FjNTQ3NDdhZqZhNGI5NWU5ZTI=&quot;&gt;
&lt;source type=&quot;video/mp4&quot; src=&quot;{{ $video->url ?? "Not Finished" }}&quot;&gt;
&lt;/video&gt;

&lt;!-- JS code --&gt;
&lt;!-- If you'd like to support IE8 (for Video.js versions prior to v7) --&gt;
&lt;script src=&quot;https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js&quot;&gt;&lt;/script&gt;
&lt;script src=&quot;https://oann-push.s3-us-west-2.amazonaws.com/video.min.js&quot;&gt;&lt;/script&gt;

&lt;script src=&quot;https://cdn.viblast.com/vb/stable/viblast.js&quot;&gt;&lt;/script&gt;
&lt;script src=&quot;https://oann-push.s3-us-west-2.amazonaws.com/plugins/vast.vpaid.min.js&quot;&gt;&lt;/script&gt;
&lt;script src=&quot;https://oann-push.s3-us-west-2.amazonaws.com/nuevo.min.js&quot;&gt;&lt;/script&gt;

&lt;script type=&quot;text/javascript&quot;&gt;
    var player = videojs('v-{{ $video->uuid }}');
    player.vastAds({ tagURL :"https://tv.springserve.com/vast/627530?w=640&h=400&cb=0000&url={{ $video->url }}&us_privacy=1&schain=1", id:"v-{{ $video->uuid }}"});
    
&lt;/script&gt;

</code>

</pre>

</div>
                </div>

  </div>

</div>



               
                
            </x-card>
        </div>
    </div>
</div>



@endsection


@push('bottom-scripts')
<script src="{{ mix('js/app.js') }}"></script>
<script src="https://vjs.zencdn.net/7.8.4/video.js"></script>
<script type="text/javascript">
    new ClipboardJS('.btn-copy')
</script>
@endpush