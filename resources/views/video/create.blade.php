@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">

            <x-card header="Upload Video">
                <livewire:uploader />
            </x-card>
        </div>
    </div>
    
</div>


@endsection

