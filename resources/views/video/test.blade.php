<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    
        {{--   <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/shaka-player/3.1.0/shaka-player.compiled.debug.js"></script> --}}

          <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/shaka-player/4.0.0/shaka-player.ui.debug.min.js"></script>

</head>
<body>
    <video id="player" width="640" height="320">

    </video>



          <script>


     var fairplayerCertificateUri = 'http://oann-push.test/cert/fairplay.cer';
     var fairplayerCertificate= "";

     function initPlayer(){

        //.shaka.log.setLevel(shaka.log.Level.INFO);

        shaka.polyfill.installAll();
     var videoPlayerElm = document.getElementById("player");
     var player = new shaka.Player(videoPlayerElm);
      player.addEventListener('error', onErrorEvent);   
     player.configure({
        drm: {
            servers: {
                'com.widevine.alpha': 'https://drm-widevine-licensing.axprod.net/AcquireLicense',
                'com.microsoft.playready': 'https://drm-playready-licensing.axprod.net/AcquireLicense',
                'com.apple.fps.1_0': 'https://drm-fairplay-licensing.axprod.net/AcquireLicense'
            }
        }
    });

      player.getNetworkingEngine().registerRequestFilter(function(type, request) {
                            if (type == shaka.net.NetworkingEngine.RequestType.LICENSE) {
                                // This is the specific header name and value the server wants:
                                request.headers['X-AxDRM-Message'] = 'eyJhbGciOiJIUzI1NiJ9.eyJ2ZXJzaW9uIjoxLCJjb21fa2V5X2lkIjoiNjNkOWU5ODQtMTA0YS00NmE5LWFhNWMtYWU2MTAxMjQxMGY3IiwibWVzc2FnZSI6eyJ0eXBlIjoiZW50aXRsZW1lbnRfbWVzc2FnZSIsInZlcnNpb24iOjIsImxpY2Vuc2UiOnsiZHVyYXRpb24iOjYyNDB9LCJjb250ZW50X2tleXNfc291cmNlIjp7ImlubGluZSI6W3siaWQiOiJiYTBlZDNlZi0wNGIzLWU1ZjMtNjUyZC0zZjhlMDdjMGUyYmEifV19LCJ3aWRldmluZSI6eyJkZXZpY2Vfc2VjdXJpdHlfbGV2ZWwiOiJIV19TRUNVUkVfQUxMIiwiY2dtc19hIjoibmV2ZXIiLCJoZGNwIjoiMi4wIiwiZGlzYWJsZV9hbmFsb2dfb3V0cHV0Ijp0cnVlfSwicGxheXJlYWR5Ijp7Im1pbl9kZXZpY2Vfc2VjdXJpdHlfbGV2ZWwiOjMwMDAsImFuYWxvZ192aWRlb19vcGwiOjMwMCwicGxheV9lbmFibGVycyI6bnVsbCwiY29tcHJlc3NlZF9kaWdpdGFsX3ZpZGVvX29wbCI6NDAwLCJ1bmNvbXByZXNzZWRfZGlnaXRhbF92aWRlb19vcGwiOjMwMH19fQ.eVuxs0ZsQWxrgfHFecIynorXTAEbqIKqVq1JUMbDrR8';
                            }
                            
                        });

     player.configure('drm.advanced.com\\.apple\\.fps\\.1_0.serverCertificate', fairplayerCertificate);

    
        player.load("https://ol-vod.oann.com/06431888-95e4-4267-8e12-546e629d0a1f/06431888-95e4-4267-8e12-546e629d0a1f.m3u8").then(function(){
            console.log("The video has loaded");
        }).catch(onError);
   
    }

      // FairPlay-specific functionality.
            function loadFairPlayCertificate() {
                console.log("Requesting FairPlay certificate from " + fairplayerCertificateUri)
                var request = new XMLHttpRequest();
                request.responseType = 'arraybuffer';
                request.addEventListener('load', onFairPlayCertificateLoaded, false);
                request.addEventListener('error', onFairPlayCertificateError, false);
                request.open('GET', fairplayerCertificateUri, true);
                request.setRequestHeader('Pragma', 'Cache-Control: no-cache');
                request.setRequestHeader("Cache-Control", "max-age=0");
                request.send();

                


            }
        
            function onFairPlayCertificateLoaded(event) {
                console.log("FairPlay certificate received.");
                var request = event.target;
                fairplayerCertificate =  new Uint8Array(request.response);
                console.log(fairplayerCertificate);
                initPlayer();
               
            }
            
            function onFairPlayCertificateError(event) {
                console.error('FairPlay certificate request failed.');
            } 


              function onErrorEvent(event) {
                onError(event.detail);
            }

            function onError(error) {
                console.error('Error code', error.code, 'object', error);
            }

            loadFairPlayCertificate();
            
    

    
    

    </script>
</body>
</html>