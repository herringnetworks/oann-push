@extends("layouts.app")


@section("content")
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
           <livewire:video.update :video="$video">
        </div>
    </div>
</div>

@endsection

@push("bottom-scripts")
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="https://vjs.zencdn.net/7.8.4/video.js"></script>
@endpush