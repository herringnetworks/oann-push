@extends('layouts.app')


@section('content')

<div class="container">

    <div class="col-md-12">


        <div class="row pl-3 mb-3">
            <div class="col-md-10">
                 <a href="{{ route('video.create') }}" class="btn btn-primary">Upload Video</a>
            </div>
           
            <div class="col-md-2">
                <a href="{{ route('video.all') }}"> View All</a> |
                <a href="{{ route('video.index') }}">My Uploads</a>
            </div>
        </div>

        <x-card header="Video list">
            <table class="table table table-responsive">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Url</th>
                        <th>Uploaded By</th>
                        <th>Uploaded Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($videos as $video)
                        <tr>
                            <td><a href="{{ url('video/'.$video->id) }}">{{ $video->title }}</a></td>
                            <td><span class="badge rounded-pill @if($video->status === 'complete') bg-success @elseif($video->status === 'error') bg-danger @else bg-secondary @endif">{{ $video->status }} </span></td>
                           <td>{{ Illuminate\Support\Str::of($video->url)->limit(70,'...') }}</td>
                            <td>{{ $video->user->email }}</td>
                            <td>{{ $video->createdDate }}</td>
                        </tr>


                    @endforeach
                </tbody>
            </table>

            {{ $videos->links() }}
        </x-card>



    </div>


</div>

@endsection



