@extends('layouts.base')


@push('styles')

<link rel="stylesheet" href="{{ url(mix('css/oann/style.css')) }}" type="text/css" >
 <link href="https://vjs.zencdn.net/8.16.1/video-js.css" rel="stylesheet" />
@endpush


@section('content')

<div class="feature-img">
<img src="{{$featureMedia}}" alt="">
    </div>
<div class="container visible-xs">
    {{-- <div class="feature-img">
        <img src="http://d2pggiv3o55wnc.cloudfront.net/oann/wp-content/uploads/2019/09/2019-09-18T152819Z_1_LYNXMPEF8H1AG_RTROPTP_0_SAUDI-ARAMCO-DEFENCE_1.jpg" alt="Remains of the missiles which were used to attack an Aramco oil facility, are displayed during a news conference in Riyadh, Saudi Arabia September 18, 2019. REUTERS/Hamad I Mohammed
">
    </div> --}}

    
    <div class="article">
        <div class="heading">
            <h1>{{$title}}</h1>
            <hr />
        </div>
        <div class="category">
            <h3>{{$category}}</h3>
        </div>

        <div class="text">
            {!! $content !!}
        </div>
    </div>
    
</div>

@endsection

@push('bottom-scripts')
<script async data-cfasync="false" src="https://cdn1.customads.co/embeds/oneamericanewsnetwork_inapp.js"></script>
<script src="https://vjs.zencdn.net/8.16.1/video.min.js"></script>
<script src="{{ url(mix('js/article.js'))}}" type="text/javascript"></script>
<script src="https://vjs.zencdn.net/8.16.1/video.min.js"></script>

@endpush