@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="row">
             <div class="col-md-8 col-lg-12">
                <x-card header="Edit User">
                    <x-edit-user user={{ $user }} />
                </x-card>
             </div>
        </div>
    </div>
   
@endsection