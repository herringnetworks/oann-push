@extends('layouts.app')


@section('content')

<div class="container">
    <div class="row  justify-content-center">
        <div class="col-md-8 col-lg-12">
            <x-card header="Users">
                <div class="table-responsive">
                    <table class="table table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Email</th>
                            <th scope="col">Role</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr scope="row">
                                <td>{{ $user->email }}</td>
                                <td>
                                    @foreach($user->getRoleNames() as $role)
                                        {{ $role }},
                                    @endforeach
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="pl-2 pr-2">
                                             <a href="{{ route('admin.user.edit',$user->id) }}" class="btn btn-success">Edit</a>
                                        </div>
                                        <div class="pl-2 pr-2">
                                            <button class="btn btn-danger">Delete</button>
                                        </div>
                                       
                                        
                                    </div>
                                    </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </x-card>
        </div>
    </div>
   
</div>



@endsection