@extends('layouts.app')

@section('content')
<div class="contianer">

    <div class="row justify-content-center">
        <div class="col-md-8 col-lg-8 col-xl-6">
            <x-card header="Edit Role">
    
          
                <h3>Role: {{ $role->name }}</h3>
              
                <div class="mt-2">
                    <h4>Add Permissions to {{ $role->name }}</h4>
                    <form method="POST" action="{{route('role.update',$role->id)}}">
                        @csrf
                        @method('PATCH')
                        @foreach($permissions as $permission)
                            <div class="form-check col">
                            <input class="form-check-input" type="checkbox" id="{{ $permission->name }}" name="permissions[]" value="{{ $permission->name }}" {{ $role->hasPermissionTo( $permission->name)?? "checked" }}/>
                                <label for="{{ $permission->name }}" class="form-check-label">{{ $permission->name }}</label>
                            </div>
                        @endforeach

                        <div class="mt-3">
                        <button class="btn btn-primary">Add Permissions</button>
                        </div>
        
                    </form>

                    
                </div>
                
    
    
            </x-card>
        </div>
    </div>
    
</div>

@endsection