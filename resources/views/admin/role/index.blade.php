@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-12">
            <x-card header="Roles">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="text-center">
                                    <th>Name</th>
                                    <th>Permissions</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($roles as $role)
                                    <tr class="text-center">
                                        <td>{{ $role->name }}</td>
                                        <td>{{ $role->permissions }}</td>
                                        <td>
                                            <div class="row justify-content-center">
                                                <div class="pl-2 pr-2">
                                                    <a class="btn btn-primary" href="{{ route('role.edit',$role->id) }}">Edit</a>
                                                </div>
    
                                                <div class="pl-2 pr-2">
                                                    <button class="btn btn-danger">Remove</button>
                                                </div>
                                            </div>
                                    </tr>
    
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    
                </div>
                

            </x-card>
            
        </div>
    </div>
    
</div>
@endsection