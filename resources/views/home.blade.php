@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
         @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
         @endif
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Create Push Article</div>
               
                <div class="card-body">
                   <p>Create the push article to only create the fast push for breaking news articles</p>

                   <a href="{{ route('push.create')}}" class="btn btn-primary">Create Push Article</a>

                    
                </div>
            </div>
        </div>

        @role('admin')
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Add User</div>

           
                    <div class="card-body">
                        <p>Create a new User for the breaking news notifications</p>
                       <a href="register" class="btn btn-primary">Create User</a>
                    </div>
                </div>
            </div>
        @endrole
    </div>

    <div class="row mt-5">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">OANN Article Image Uploader</div>
                <div class="card-body">
                    <p>Faster way to upload Images to articles</p>
                    <a href="{{route('image.uploader')}}" class="btn btn-primary">Upload Images</a>
                </div>
            </div>
        </div>

         <div class="col-md-6">
            <div class="card">
                <div class="card-header">Upload Video</div>
                <div class="card-body">
                    <p>Upload Videos to OANN </p>
                    <a href="{{route('video.index')}}" class="btn btn-primary">Upload Video</a>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
