<div class="card">
    <div class="card-header">{{ __('Create Push Notification') }}</div>
    <div class="card-body">
        <div class="row" x-data="{ title:'' }">
            <div class="col">
                <form  class="push-form" wire:submit.prevent="submitNotification">

                    <h3>1. Audience</h3>
                    <div class="form-check mb-3">
                        <input type="radio" class="form-check-input @error('segments') is-invalid @enderror" name="segments" value="Test Users" wire:model="segments" />
                        <label class="form-check-label" for="push-audience">Test Users </label>
                        
                    </div>

                    <div class="form-check mb-3">
                        <input type="radio" class="form-check-input @error('segments') is-invalid @enderror" name="segments" value="All" wire:model="segments" x-data x-/>
                        <label class="form-check-label" for="push-audience">All Users</label>
                        @error('segments')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        
                    </div>

                    <h3>2. Message</h3>
                    <label for="title" class="form-label">Title</label>
                    <x-input.with-emojipicker inputId="titleEmoji" />

                     <div class="mb-3">
                        <label for="subtitle" class="form-label">Subtitle</label>
                        <input type="text" class="form-control @error('subtitle') is-invalid @enderror" placeholder="Subtitle" name="subtitle" wire:model="subtitle" />
                        @error('subtitle')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="content" class="form-label">Message</label>
                        @error('content')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        <textarea name="content" id="content"  cols="30" rows="10" class="form-control @error('content') is-invalid @enderror" wire:model="content" placeholder="{{ $content }}"></textarea>
                        
                    </div>

                    <div class="mb-3">
                        <label for="link" class="form-label">Launch URL</label>
                        <input type="text" class="form-control @error('link') is-invalid @enderror" placeholder="http://bit.ly/abc" id="link" name="link" wire:model="link">
                        @error('link')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="mb-4">
                        <input type="submit" class="btn btn-primary" value="Send Push"/>
                    </div>
                </form>
            </div>
            <div class="col" x-data="{deviceType:'android' }">
                <div class="notification-preview">
                    <div class="notification-preview-wrapper">
                        <div class="phone" :class="deviceType == 'android' ? 'android': 'ios'">
                            <div class="notification" x-data="{ minified: false }">
                                <div class="notification-wrapper">
                                    <div class="notification-icon ">
                                        <div class="notification-icon-wrapper">
                                            <i :class="deviceType == 'android'?'bi-bell-fill android':'bi-apple ios'" ></i>
                                        </div>
                                    </div>
                                    <header class="notification-header" :class="deviceType == 'ios'?'no-show':''">
                                        <div class="notification-header-wrap">
                                            <span>Oan Live &#8226 now</span>
                                        </div>
                                    </header>
                                    <div class="notification-collapse" x-on:click="minified = !minified" :class="deviceType == 'ios'?'no-show':''">
                                        <i x-bind:class="minified? 'bi-chevron-down': 'bi-chevron-up'"></i>
                                    </div>
                                    <h1 class="notification-title" :class="(title.length > 0  && deviceType != 'ios')? '':'no-show'" >
                                        {{$title}}
                                    </h1>
                                   
                                    <div class="notification-body" :class="deviceType == 'ios'?'no-show':''">
                                        <p>{{ $content }}</p>
                                    </div>

                                    <div class="notification-header-body" :class="deviceType == 'android'?'no-show':''">

                                        <h1 class="notification-title" :class="title.length > 0 ? '':'no-show'">
                                            {{ $title }}
                                        </h1>
                                         @if(!empty($subtitle))
                                         <h1 class="notification-subtitle" :class="subtitle !== ''? '':'no-show'">
                                            {{ $subtitle }}
                                        </h1>
                                        @endif

                                        <div class="notification-body">
                                            <p class="notification-text">{{ $content }}</p>
                                        </div>
                                    </div>

                                    <div class="notification-time-thumbnail" :class="deviceType == 'android'?'no-show':''">
                                        <div class="notification-time">
                                            now
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="notification-device-picker">
                            <select name="" id="" class="form-select form-select-md mb-3" x-model="deviceType">
                                <option selected value="android">Android</option>
                                <option value="ios">iOS</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
</div>

@push('bottom-scripts')
<script src="https://cdn.jsdelivr.net/npm/emoji-mart@latest/dist/browser.js"></script>
<script>
 

  //document.body.appendChild(picker)
</script>

@endpush
