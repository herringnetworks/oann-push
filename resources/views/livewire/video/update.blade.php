<div>
    <div class="row">
         <div class="col-md-8">
            <x-card header="Video">
                <form method="POST"  wire:submit.prevent="updateVideo">
                    @csrf
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" name="title" id="title" wire:model="title"  />
                    </div>
                    
                    <div class="form-group">
                        <label>Description</label>
                        
                        <textarea name="description" id="description" col="2" row="9" class="form-control" wire:model="description" ></textarea>
                        
                        
                    </div>
                    
                    <div class="mt-3">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>

                </form>
            </x-card>
        </div>

        <div class="col-md-4">
            <x-video-card video-url="{{$video->url}}">
                <h3>{{$video->title}}</h3>
                <ul class="pt-2">
                    <li>Filename: {{ $video->title }}</li>
                    <li><div class="text-truncate">url: <a href="{{ $video->url }}">{{$video->url}}</a></div> </li>
                </ul>
            
            </x-video-card>
        </div>
    </div>
   
    



</div>
