@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <form method="POST" action="{{ route('live.fb.update') }}">
                                        @csrf

                                        @if($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="form-group">
                                            @if(isset($success))
                                            <div class="alert alert-danger">Fb Stream has been updated!</div>
                                            @endif

                                            <label for="fbstreamskey">Facebook Stream Key</label>
                                            <input type="text" class="form-control {{ $errors->has('key') ? 'is-invalid':''}}" id="key" name="key" required>
                                            <small class="form-text text-muted">Please add in follow format 00000000000?s_bl=1&s_ps=1&s_sml=3&s_sw  </small>
                                            @if($errors->has('key'))
                                                <div class="invalid-feedback">
                                                    {{$errors->first('key')}}
                                                </div>
                                            @endif
                                        </div>

                                        <input type="submit" class="btn btn-primary" value="Update Feed" />

                                    </form>



                                </div>

                            </div>

                            <div class="row mt-4">
                                <div class="col-md-6">
                                <form action="{{route('live.fb.restart') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="restartRtmp">Restart Live Feed</label>
                                        <br/>
                                        <button type="submit" class="btn btn-secondary" name="restartRtmp" id="restartRtmp">Restart Feed</button>
                                    </div>
                                </form>
                            </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    <div class="row">
                                        <div id="clockdiv" class="green hide">
                                            <span class="hours"></span>:
                                            <span class="minutes"></span>:
                                            <span class="seconds"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <button id="resetTimer" class="btn btn-info">Reset Timer 8 hours</button>
                                </div>
                            </div>
                        </div>



                    <div class="col-md-6 col-lg-6">
                        <p><b>id:</b> {{$republish['id']}} <br/>
                           <b>src_app:</b> {{$republish['src_app']}} <br/>
                           <b>src_strm:</b> {{$republish['src_strm']}} <br/>
                           <b>dest_addr:</b> {{$republish['dest_addr']}} <br/>
                           <b>dest_port:</b> {{$republish['dest_port']}} <br/>
                           <b>dest_app:</b> {{$republish['dest_app']}} <br/>
                           <b>dest_strm</b> {{$republish['dest_strm']}} <br/>
                           <b>dest_app_params:</b> {{$republish['dest_app_params']}} <br/>
                           <b>auth_schema:</b> {{$republish['auth_schema']}} <br/>
                           <b>dest_login:</b> {{$republish['dest_login']}} <br/>
                           <b>dest_password:</b> {{$republish['dest_password']}} <br/>
                           <b>dest_app_params:</b> {{$republish['dest_app_params']}} <br/>
                           <b>dest_strm_params:</b> {{$republish['dest_strm_params']}} <br/>
                           <b>paused:</b>{{$republish['paused']}}<br/>
                           <b>keep_src_stream_params:</b>{{$republish['keep_src_stream_params']}}<br/>
                           <b>ssl:</b>{{$republish['ssl']}}<br/>
                        </p>
                    </div>
                    </div>

                    <div class="row">

                    </div>

                </div>

            </div>
        </div>
    </div>
</div>




@endsection

@push('bottom-scripts')
    <script src="{{mix('js/app.js')}}"></script>
    <script src="{{mix('js/FBStream.js')}}"></script>


@endpush
