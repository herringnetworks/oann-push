@extends('layouts.app')



@section('content')
<x-container>

    

    <x-card header="Upload Images">
        <div class="row">
            <div class="col-md-12">
                <p>*Drag and Drop images check the site if image has been uploaded to the site</p>
            </div>
          
        </div>
        <form method="POST" action="/image/upload" class="dropzone"  id="uploader" enctype="multipart/form-data">
    
        @csrf
        
        </form>

    </x-card>
</x-container>

@endsection