@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
                            @if(Session::has('error'))
                    <div class="alert alert-danger">

                        {{ Session::get('error') }}
                    </div>
                @elseif(Session::has('success'))

                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
                <div class="card">
    <div class="card-header">{{ __('Create Push Article') }}</div>

    <div class="card-body bg-white">
        <form method="POST" action="{{ route('push.save') }}" class="push-form">
            @csrf

            <div class="form-group row mb-4">
                <label for="title" class="col-md-4 col-form-label text-md-end">{{ __('Title') }}</label>

                <div class="col-md-6">
                    <input id="title" type="text" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" data-emojiable="converted" value="{{ old('title') }}" required autofocus >
                   
                    @if ($errors->has('title'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row mb-4">
                <label for="content" class="col-md-4 col-form-label text-md-end">{{ __('Content') }}</label>

                <div class="col-md-6">
                    {{--  <input id="content" type="text" class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }}" name="content" value="{{ old('content') }}" required autofocus> --}}

                    <textarea id="content" type="text" rows="8" class="form-control {{ $errors->has('content') ? 'is-invalid':''}}" name="content" value="{{ old('content') }}">

                    </textarea>

                    @if ($errors->has('content'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('content') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Create Push Article') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
    </div>

        </div>
    </div>
</div>


@endsection


@push('bottom-scripts')
 <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
 <script src="{{ asset('lib/js/config.js') }}"></script>
 <script src="{{ asset('lib/js/util.js') }}"></script>
 
 
@endpush

